#!/usr/bin/env bash

#
# Copyright (c) 2022 Systao
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

function usage() {
  echo "Usage: ./perform-release.sh -t <tag>"
  echo "Options: "
  echo '  -t,--tag      Target tag. Tag must exist and branch releases/v$tag must exist and not be protected'
  echo '  -a,--apply    Disable dry-run mode and write changes to the repository. Default=false'
}

function verify() {
  if ! git show-ref --quiet refs/tags/$tag; then
    echo "ERR: tag $tag doesn't exist"
    exit 1
  fi

  git checkout $branch 1> /dev/null 2>&1 # force checkout of the branch
  if ! git show-ref --quiet refs/heads/$branch; then
    echo "ERR: branch $branch doesn't exist"
    exit 1
  fi

  if [[ "$(git diff $branch $tag)" != "" ]]; then
    echo "ERR: branch $branch and tag $tag diverge"
    exit 1
  fi

  git checkout $tag
  if [[ "$current_version" != "$tag" ]]; then
    echo "ERR: \${project.version} $current_version does not match tag $tag"
    exit 1
  fi
}

function rebase_master() {
  git checkout master
  git pull
  git rebase $branch

  if [[ "$dry_run" == "" ]]; then
    echo "Mode dry-run is disabled (option -a). Pushing master to remote"
    git push
  fi
}

function protect_branch() {
  curl --header "PRIVATE-TOKEN: ${token}" \
       --request POST \
       --data "name=releases/v${tag}&id=${project_id}&push_access_level=0&merge_access_level=0" \
       https://gitlab.com/api/v4/projects/${project_id}/protected_branches
}

dry_run="-DdryRun=true"
while getopts at:k:p:-: arg; do
  case $arg in
    a ) dry_run="" ;;
    t ) tag="$OPTARG" ;;
    k ) token="$OPTARG" ;;
    p ) project_id="$OPTARG" ;;
    \? ) exit 2 ;;  # getopts already reported the illegal option
  esac
done
shift $((OPTIND-1)) # remove parsed options and args from $@ list

branch=releases/$tag
current_version=`mvn help:evaluate -Dexpression=project.version -q -DforceStdout`


echo "Parameters:"
echo -n "tag=$tag | "
echo -n "branch=$branch | "
echo -n "current_version=$current_version | "
echo -n "project_id=$project_id | "
echo "dry_run=${dry_run:-unset}"

git pull --all --tags

verify
rebase_master
protect_branch

echo "Done! All good!"
