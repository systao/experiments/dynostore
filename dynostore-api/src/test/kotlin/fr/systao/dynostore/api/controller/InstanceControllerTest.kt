/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import fr.systao.dynostore.api.ApiTest
import fr.systao.dynostore.api.config.OIDCConfiguration
import fr.systao.dynostore.api.filter.CurlConverterFilterBean
import fr.systao.dynostore.api.fixtures.ContextHelper
import fr.systao.dynostore.api.fixtures.JWTHelper
import fr.systao.dynostore.api.model.*
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.service.ScopeService
import fr.systao.dynostore.core.domain.entity.service.StoreEntityService
import fr.systao.dynostore.core.domain.instance.InconsistentInstanceQueryException
import fr.systao.dynostore.core.domain.instance.InstanceNotFoundException
import fr.systao.dynostore.core.domain.instance.model.Instance
import fr.systao.dynostore.core.domain.instance.model.SyntheticKey
import fr.systao.dynostore.core.domain.instance.service.InstanceService
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import org.springframework.test.web.servlet.result.ContentResultMatchersDsl
import org.springframework.web.context.WebApplicationContext
import java.util.*

@ApiTest(
    imports=[OIDCConfiguration::class, InstanceMapperImpl::class, PropertyValueMapperImpl::class, SingleValueMapperImpl::class, CurlConverterFilterBean::class],
    controllers=[InstanceController::class]
)
class InstanceControllerTest {

    @MockkBean
    private lateinit var service: InstanceService

    @MockkBean
    private lateinit var storeEntityService: StoreEntityService

    @MockkBean
    private lateinit var scopeService: ScopeService

    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @Autowired
    private lateinit var oidcConfiguration: OIDCConfiguration

    @BeforeEach
    fun setUp() {
        oidcConfiguration.userIdClaim = "sub"
        mvc = ContextHelper.setupMockMvc(context)
    }

    @ParameterizedTest(name = "When {2} is passed should return {1}")
    @MethodSource("testCreateInstanceParameters")
    fun `POST instance endpoint requires proper authentication`(jwt: String?, status: Int, message: String) {
        val entity = StoreEntity("test")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        val property = Property("test1")
        val propertyId = UUID.randomUUID()
        property.id = propertyId

        entity.properties = mutableSetOf(property)

        val instance = Instance(entity)
        val instanceId = UUID.randomUUID()
        instance.owner = scope
        instance.id = instanceId

        every { scopeService.loadScope(scopeId) } returns scope
        every { storeEntityService.find(scopeId, entityId) } returns entity
        every { service.create(scope, entity, any()) } returns instance

        val result = postInstance(jwt, scopeId, entityId, listOf())

        result
            .andDo { print() }
            .andExpect {
                status { isEqualTo(status) }
            }

        if ( status == 200 ) {
            result
                .andDo { print() }
                .andExpect {
                    content(matchesResponse(instanceId, entityId, scopeId, "test.test"))
                }
        }
    }

    @Test
    fun `Given an existing instance of id $id in a scope $scopeId get instance($scopeId, $entityId, $id) returns it`() {
        val entity = StoreEntity("test")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        val instance = Instance(entity)
        val instanceId = UUID.randomUUID()
        instance.owner = scope
        instance.id = instanceId

        every { service.find(SyntheticKey(scopeId, entityId, instanceId)) } returns instance

        mvc
            .get("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer " + JWTHelper.createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    content(matchesResponse(instanceId, entityId, scopeId, "test.test"))
                }
            }
    }

    @Test
    fun `Trying to retrieve a non existing instance returns a 404`() {
        val entityId = UUID.randomUUID()
        val instanceId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.find(SyntheticKey(scopeId, entityId, instanceId)) } throws InstanceNotFoundException(instanceId)

        mvc
            .get("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer " + JWTHelper.createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("INSTANCE_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an instance that doesn't exist returns a 404`() {
        val jwt = JWTHelper.createToken()
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()
        val instanceId = UUID.randomUUID()

        every { service.delete(SyntheticKey(scopeId, entityId, instanceId)) } throws InstanceNotFoundException(instanceId)

        mvc
            .delete("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer $jwt"
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("INSTANCE_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an entity that doesn't belong to the given scope throws a 400`() {
        val jwt = JWTHelper.createToken()
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()
        val instanceId = UUID.randomUUID()

        every { service.delete(SyntheticKey(scopeId, entityId, instanceId)) } throws InconsistentInstanceQueryException("any")

        mvc
            .delete("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer $jwt"
                }
            }
            .andDo { print() }
            .andExpect {
                status { isBadRequest() }
                content {
                    jsonPath("$.code") {
                        value("INCONSISTENT_QUERY")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an existing instance returns a 204`() {
        val instanceId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()
        val entityId = UUID.randomUUID()

        every { service.delete(SyntheticKey(scopeId, entityId, instanceId)) } just Runs

        mvc
            .delete("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer " + JWTHelper.createToken()
                }
            }
            .andDo { print() }
            .andExpect { status { isNoContent() } }
    }

    @Test
    fun `Setting the value of a property returns the updated instance`() {
        val entity = StoreEntity("test")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        val property = Property("testProperty")
        val propertyId = UUID.randomUUID()
        property.id = propertyId

        entity.properties = mutableSetOf(property)

        val instance = Instance(entity)
        val instanceId = UUID.randomUUID()
        instance.owner = scope
        instance.id = instanceId

        every { storeEntityService.find(scopeId, entityId) } returns entity
        every { service.setProperty(any(), "testProperty", any()) } returns instance

        mvc
            .put("/domain/scopes/$scopeId/entities/$entityId/instances/$instanceId") {
                headers {
                    this["Authorization"] = "Bearer " + JWTHelper.createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = "{\"name\": \"testProperty\", \"value\": { \"stringValue\": \"newValue\" } }"
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    content(matchesResponse(instanceId, entityId, scopeId, "test.test"))
                    jsonPath("properties") {
                        isArray()
                    }
                }
            }
    }

    private fun postInstance(jwt: String?, scopeId: UUID, entityId: UUID, properties: List<PropertyValueDto>): ResultActionsDsl {
        return mvc.post("/domain/scopes/$scopeId/entities/$entityId/instances") {
            contentType = MediaType.APPLICATION_JSON
            content = ScopeControllerTest.mapper.writeValueAsString(InstanceRequest(scopeId, properties))
            accept = MediaType.APPLICATION_JSON
            headers {
                this["Authorization"] = "Bearer $jwt"
            }
        }
    }

    private fun MockMvcResultMatchersDsl.matchesResponse(
        instanceId: UUID,
        entityId: UUID,
        scopeId: UUID,
        entityPath: String): ContentResultMatchersDsl.() -> Unit = {
        jsonPath("$.id") {
            value(instanceId.toString())
        }
        jsonPath("$.entityId") {
            value(entityId.toString())
        }
        jsonPath("$.scopeId") {
            value(scopeId.toString())
        }
        jsonPath("$.entity") {
            value(entityPath)
        }
        jsonPath("$.properties") {
            isArray()
        }
    }

    companion object {
        val mapper : ObjectMapper = ObjectMapper()

        @JvmStatic
        private fun testCreateInstanceParameters() : List<Arguments> {
            return listOf(
                Arguments.of("not.a.valid.jwt", 401, "invalid jwt"),
                Arguments.of(null, 401, "null jwt"),
                Arguments.of(JWTHelper.createToken(), 200, "valid jwt"),
            )
        }

    }

}