/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.config

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEqualTo
import fr.systao.dynostore.api.fixtures.JWTHelper
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.util.*
import java.util.stream.Stream

class DynostorePrincipalImplTest {

    @ParameterizedTest
    @MethodSource("testAuthoritiesParameters")
    fun testAuthorities(roles: List<String>?, authorities: Array<GrantedAuthority>) {
        val jwt = JWTHelper.claimSet("sub", roles)
        val principal = DynostorePrincipalImpl(jwt, "sub")
        assertThat(principal.authorities()).containsExactlyInAnyOrder(*authorities)
    }

    @Test
    fun testId() {
        val sub = UUID.randomUUID().toString()
        val jwt = JWTHelper.claimSet("sub", id = sub)
        val principal = DynostorePrincipalImpl(jwt, "sub")
        assertThat(principal.id()).isEqualTo(sub)
    }

    companion object {
        @JvmStatic
        fun testAuthoritiesParameters() : Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    listOf("role1", "role2"),
                    arrayOf(SimpleGrantedAuthority("ROLE_ROLE1"), SimpleGrantedAuthority("ROLE_ROLE2"))
                ),
                Arguments.of(
                    listOf<String>(),
                    arrayOf<GrantedAuthority>()
                ),
                Arguments.of(
                    null,
                    arrayOf<GrantedAuthority>()
                )
            )
        }
    }
}