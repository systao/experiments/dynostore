/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.fixtures

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSObject
import com.nimbusds.jose.Payload
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import com.nimbusds.jwt.JWTClaimsSet
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import java.util.*

class ContextHelper {
    companion object {
        @JvmStatic
        fun setupMockMvc(context: WebApplicationContext): MockMvc {
            return MockMvcBuilders
                .webAppContextSetup(context)
                .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
                .build()
        }
    }
}

class JWTHelper {
    companion object {
        @JvmStatic
        fun claimSet(userClaimName: String = "sub", roles: List<String>? = listOf(), id: String? = null) : JWTClaimsSet {
            return JWTClaimsSet.Builder()
                .claim(userClaimName, id ?: UUID.randomUUID().toString())
                .claim("email", "john.doe@mail.com")
                .claim("name", "John Doe")
                .claim("roles", roles)
                .build()
        }

        @JvmStatic
        fun createToken(userClaimName: String = "sub", roles: List<String>? = listOf()) : String {
            val claims = claimSet(userClaimName, roles)

            val rsaJWK: RSAKey = RSAKeyGenerator(2048)
                .keyID("key-id")
                .generate()

            val jws = JWSObject(
                JWSHeader.Builder(JWSAlgorithm.RS256).keyID(rsaJWK.keyID).build(),
                Payload(claims.toJSONObject())
            )

            jws.sign(RSASSASigner(rsaJWK))

            return jws.serialize()
        }
    }
}