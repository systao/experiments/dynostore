/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import fr.systao.dynostore.api.ApiTest
import fr.systao.dynostore.api.config.OIDCConfiguration
import fr.systao.dynostore.api.filter.CurlConverterFilterBean
import fr.systao.dynostore.api.fixtures.ContextHelper
import fr.systao.dynostore.api.fixtures.JWTHelper.Companion.createToken
import fr.systao.dynostore.api.model.*
import fr.systao.dynostore.core.domain.entity.EntityNotFoundException
import fr.systao.dynostore.core.domain.entity.InconsistentQueryException
import fr.systao.dynostore.core.domain.entity.NoSuchPropertyException
import fr.systao.dynostore.core.domain.entity.PropertyConflictException
import fr.systao.dynostore.core.domain.entity.model.*
import fr.systao.dynostore.core.domain.entity.service.StoreEntityService
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import org.springframework.test.web.servlet.result.ContentResultMatchersDsl
import org.springframework.web.context.WebApplicationContext
import java.util.*

@ApiTest(
    imports=[OIDCConfiguration::class, EntityMapperImpl::class, PropertyMapperImpl::class, CurlConverterFilterBean::class],
    controllers=[EntityController::class]
)
class EntityControllerTest {

    @MockkBean
    private lateinit var service: StoreEntityService

    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @Autowired
    private lateinit var oidcConfiguration: OIDCConfiguration

    @BeforeEach
    fun setUp() {
        oidcConfiguration.userIdClaim = "sub"
        mvc = ContextHelper.setupMockMvc(context)
    }

    @ParameterizedTest(name = "When {2} is passed should return {1}")
    @MethodSource("testCreateEntityParameters")
    fun `POST entities endpoint requires proper authentication`(jwt: String?, status: Int, @Suppress("unused") message: String) {
        val entity = StoreEntity("test")
        val id = UUID.randomUUID()
        entity.id = id

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        every { service.create("test", scopeId, mutableSetOf()) } returns entity

        val result = postEntity(jwt, scopeId, mutableSetOf())

        result
            .andDo { print() }
            .andExpect {
                status { isEqualTo(status) }
            }

        if ( status == 200 ) {
            result
                .andDo { print() }
                .andExpect {
                    content(matchesResponse(id, scopeId, "test", "test.test"))
                }
        }
    }

    @Test
    fun `Trying to delete an entity that doesn't exist returns a 404`() {
        val jwt = createToken()
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.delete(id, scopeId) } throws EntityNotFoundException(id)

        mvc
            .delete("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer $jwt"
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("ENTITY_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an entity that doesn't belong to the given scope throws a 400`() {
        val jwt = createToken()
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.delete(id, scopeId) } throws InconsistentQueryException("any")

        mvc
            .delete("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer $jwt"
                }
            }
            .andDo { print() }
            .andExpect {
                status { isBadRequest() }
                content {
                    jsonPath("$.code") {
                        value("INCONSISTENT_QUERY")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an existing entity returns a 204`() {
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.delete(id, scopeId) } just Runs

        mvc
            .delete("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect { status { isNoContent() } }
    }

    @Test
    fun `Given an existing entity of id $id in a scope $scopeId get entity($scopeId, $id) returns it`() {
        val entity = StoreEntity("test")
        val id = UUID.randomUUID()
        entity.id = id

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        every { service.find(scopeId, id) } returns entity

        mvc
            .get("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    content(matchesResponse(id, scopeId, "test", "test.test"))
                }
            }
    }

    @Test
    fun `Trying to retrieve a non existing entity returns a 404`() {
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.find(scopeId, id) } throws EntityNotFoundException(id, scopeId)

        mvc
            .get("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("ENTITY_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to rename a non existing entity returns a 404`() {
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.rename(id, scopeId, "test") } throws EntityNotFoundException(id, scopeId)

        mvc
            .patch("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test"))
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("ENTITY_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to rename an existing entity that doesn't belong to the given scope returns a 400`() {
        val id = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.rename(id, scopeId, "test") } throws InconsistentQueryException("any")

        mvc
            .patch("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test"))
            }
            .andDo { print() }
            .andExpect {
                status { isBadRequest() }
                content {
                    jsonPath("$.code") {
                        value("INCONSISTENT_QUERY")
                    }
                }
            }
    }

    @Test
    fun `Updating an existing entity returns the renamed entity`() {
        val entity = StoreEntity("test2")
        val id = UUID.randomUUID()
        entity.id = id

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        every { service.rename(id, scopeId, "test2") } returns entity

        mvc
            .patch("/domain/scopes/$scopeId/entities/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test2"))
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content(matchesResponse(id, scopeId, "test2", "test.test2"))
            }
    }

    @Test
    fun `Trying to rename a property with a name of an existing property returns a 409`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()
        val propertyId = UUID.randomUUID()

        every { service.renameProperty(PropertyKey(scopeId, entityId, propertyId), "test2") } throws PropertyConflictException(propertyId, listOf("test2"))

        val requestBody = mapper.writeValueAsString(RenamePropertyRequest(name = "test2"))

        runPropertyRequest(
            entityId,
            scopeId,
            propertyId,
            mvc.patch("/domain/scopes/$scopeId/entities/$entityId/properties/$propertyId", dsl = writePropertyRequest(requestBody)),
            "PROPERTY_EXISTS"
        ).andExpect {
            status { isConflict() }
        }
    }

    @Test
    fun `Trying to rename a non existing property returns a 404`() {
        val entity = StoreEntity("test2")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId
        entity.setOwner(scope)

        val propertyId = UUID.randomUUID()

        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        every { service.renameProperty(PropertyKey(scopeId, entityId, propertyId), "test4") } throws NoSuchPropertyException(propertyId, "test2")

        mvc
            .patch("/domain/scopes/$scopeId/entities/$entityId/properties/$propertyId") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test4"))
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("PROPERTY_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to rename a property of an entity that doesn't belong to the passed scope returns a 400`() {
        val entity = StoreEntity("test2")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scopeId = UUID.randomUUID()

        val propertyId = UUID.randomUUID()

        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        every { service.renameProperty(PropertyKey(scopeId, entityId, propertyId), "test4") } throws InconsistentQueryException("any")

        mvc
            .patch("/domain/scopes/$scopeId/entities/$entityId/properties/$propertyId") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test4"))
            }
            .andDo { print() }
            .andExpect {
                status { isBadRequest() }
                content {
                    jsonPath("$.code") {
                        value("INCONSISTENT_QUERY")
                    }
                }
            }
    }

    @Test
    fun `Trying to rename an existing property of an entity that doesn't have an homonym property actually rename it`() {
        val entity = StoreEntity("test2")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scopeId = UUID.randomUUID()

        val property = Property("test1")
        val propertyId = UUID.randomUUID()
        property.id = propertyId

        entity.properties = mutableSetOf(property, Property("test2"), Property("test3"))

        val updated = Property("test4")
        property.id = propertyId

        every { service.renameProperty(PropertyKey(scopeId, entityId, propertyId), "test4") } returns updated

        mvc
            .patch("/domain/scopes/$scopeId/entities/$entityId/properties/$propertyId") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test4"))
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$.id") {
                        doesNotExist()
                    }
                    jsonPath("$.multiplicity") {
                        value(1)
                    }
                    jsonPath("$.name") {
                        value("test4")
                    }
                }
            }
    }


    @Test
    fun `Trying to add a property with a name of an existing property returns a 409`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.addProperties(entityId, scopeId, any()) } throws PropertyConflictException(entityId, listOf("test2"))

        val requestBody = mapper.writeValueAsString(listOf(PropertyDto(name = "test2", null, null, null, null)))

        runPropertyRequest(
            entityId,
            scopeId,
            null,
            mvc.post("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody)),
            "PROPERTY_EXISTS"
        ).andExpect {
            status { isConflict() }
        }
    }

    @Test
    fun `Trying to add simultaneously two properties with the same name returns a 409`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.addProperties(entityId, scopeId, any()) } throws PropertyConflictException(entityId, listOf("test2"))

        val requestBody = mapper.writeValueAsString(listOf(
            PropertyDto(name = "test2", null, null, null, null),
            PropertyDto(name = "test2", null, null, null, null)))

        runPropertyRequest(
            entityId,
            scopeId,
            null,
            mvc.post("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody)),
            "PROPERTY_EXISTS"
        ).andExpect {
            status { isConflict() }
        }
    }

    @Test
    fun `Trying to add a property to an entity that doesn't exist returns a 404`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.addProperties(entityId, scopeId, any()) } throws EntityNotFoundException(entityId)

        val requestBody = mapper.writeValueAsString(listOf(
            PropertyDto(name = "test2", null, null, null, null)))

        runPropertyRequest(
            entityId,
            scopeId,
            null,
            mvc.post("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody)),
            "ENTITY_NOT_FOUND"
        ).andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `Trying to add a property to an entity that doesn't match the given scope returns a 400`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        every { service.addProperties(entityId, scopeId, any()) } throws InconsistentQueryException("any")

        val requestBody = mapper.writeValueAsString(listOf(
            PropertyDto(name = "test2", null, null, null, null)))

        runPropertyRequest(
            entityId,
            scopeId,
            null,
            mvc.post("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody)),
            "INCONSISTENT_QUERY"
        ).andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    fun `Adding a property to an entity returns the updated entity`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()
        val propertyId = UUID.randomUUID()

        val entity = StoreEntity("e1")
        entity.id = entityId

        val scope = Scope("s1")
        scope.id = scopeId
        entity.setOwner(scope)

        val property = Property("testProperty").setPrimitiveType(PrimitiveType.STRING)
        property.id = propertyId

        entity.addProperty(property)

        every { service.addProperties(entityId, scopeId, any()) } returns entity

        val requestBody = mapper.writeValueAsString(
            listOf(PropertyDto(name = "testProperty", PrimitiveType.STRING, null, null, null)))

        mvc.post("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody))
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$.name") {
                        value("e1")
                    }
                    jsonPath("$.id") {
                        value(entityId.toString())
                    }
                    jsonPath("$.scopeId") {
                        value(scopeId.toString())
                    }
                    jsonPath("$.fqn") {
                        value("s1.e1")
                    }
                    jsonPath("$.properties") {
                        isArray()
                    }
                    jsonPath("$.properties[0].id") {
                            doesNotExist()
                    }
                    jsonPath("$.properties[0].multiplicity") {
                        value(1)
                    }
                    jsonPath("$.properties[0].name") {
                        value("testProperty")
                    }
                    jsonPath("$.properties[0].primitiveType") {
                        value("STRING")
                    }
                }
            }
    }

    @Test
    fun `Deleting properties from an entity returns a 204`() {
        val entityId = UUID.randomUUID()
        val scopeId = UUID.randomUUID()

        val entity = StoreEntity("e1")
        entity.id = entityId

        val scope = Scope("s1")
        scope.id = scopeId
        entity.setOwner(scope)

        every { service.removeProperties(entityId, scopeId, listOf("testProperty", "testProperty2")) } returns entity

        val requestBody = mapper.writeValueAsString(
            listOf("testProperty", "testProperty2")
        )

        mvc.delete("/domain/scopes/$scopeId/entities/$entityId/properties", dsl = writePropertyRequest(requestBody))
            .andDo { print() }
            .andExpect {
                status { isNoContent() }
            }
    }

    private fun postEntity(jwt: String?, scopeId: UUID, properties: MutableSet<PropertyDto>): ResultActionsDsl {
        return mvc.post("/domain/scopes/$scopeId/entities") {
            contentType = MediaType.APPLICATION_JSON
            content = ScopeControllerTest.mapper.writeValueAsString(EntityRequest(name = "test", properties = properties))
            accept = MediaType.APPLICATION_JSON
            headers {
                this["Authorization"] = "Bearer $jwt"
            }
        }
    }

    private fun MockMvcResultMatchersDsl.matchesResponse(
        entityId: UUID,
        scopeId: UUID,
        entityName: String,
        entityPath: String): ContentResultMatchersDsl.() -> Unit = {
            jsonPath("$.name") {
                value(entityName)
            }
            jsonPath("$.id") {
                value(entityId.toString())
            }
            jsonPath("$.scopeId") {
                value(scopeId.toString())
            }
            jsonPath("$.fqn") {
                value(entityPath)
            }
            jsonPath("$.properties") {
                isArray()
            }
    }

    private fun runPropertyRequest(
        entityId: UUID,
        scopeId: UUID,
        propertyId: UUID?,
        resultActionsDsl: ResultActionsDsl,
        code: String) : ResultActionsDsl {
        val entity = StoreEntity("test2")
        entity.id = entityId

        val scope = Scope("test")
        scope.id = scopeId
        entity.setOwner(scope)

        val property = Property("test1")
        property.id = propertyId

        entity.properties = mutableSetOf(property, Property("test2"), Property("test3"))

        return resultActionsDsl
            .andDo { print() }
            .andExpect {
                content {
                    jsonPath("$.code") {
                        value(code)
                    }
                }
            }
    }

    private fun writePropertyRequest(body: String): MockHttpServletRequestDsl.() -> Unit = {
        headers {
            this["Authorization"] = "Bearer " + createToken()
        }
        contentType = MediaType.APPLICATION_JSON
        content = body
        characterEncoding = "UTF-8"
    }

    companion object {
        val mapper : ObjectMapper = ObjectMapper()

        @JvmStatic
        private fun testCreateEntityParameters() : List<Arguments> {
            return listOf(
                Arguments.of("not.a.valid.jwt", 401, "invalid jwt"),
                Arguments.of(null, 401, "null jwt"),
                Arguments.of(createToken(), 200, "valid jwt"),
            )
        }

    }
}