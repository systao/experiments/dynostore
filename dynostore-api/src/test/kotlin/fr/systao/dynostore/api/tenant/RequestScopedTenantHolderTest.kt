/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.tenant

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import org.junit.jupiter.api.Test
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.http.HttpServletRequest


class RequestScopedTenantHolderTest {
    @Test
    fun `tenant fallbacks to public`() {
        unmockkStatic(RequestContextHolder::getRequestAttributes)
        val request = mockk<HttpServletRequest>()
        every { request.requestURI } returns "https://test.domain.com/e/t/c"
        val holder = RequestScopedTenantHolder()
        assertThat(holder.tenant()).isEqualTo("public")
    }

    @Test
    fun `Tenant should match subdomain`() {
        val request = mockk<HttpServletRequest>()
        every { request.requestURL } returns StringBuffer("https://test.domain.com/e/t/c")
        mockkStatic(RequestContextHolder::getRequestAttributes)
        every { RequestContextHolder.getRequestAttributes() } returns ServletRequestAttributes(request)
        val holder = RequestScopedTenantHolder()
        assertThat(holder.tenant()).isEqualTo("test")
    }

}

