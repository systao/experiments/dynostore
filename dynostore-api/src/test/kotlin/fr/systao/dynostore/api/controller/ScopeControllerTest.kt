/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import fr.systao.dynostore.api.ApiTest
import fr.systao.dynostore.api.config.OIDCConfiguration
import fr.systao.dynostore.api.fixtures.ContextHelper
import fr.systao.dynostore.api.fixtures.JWTHelper.Companion.createToken
import fr.systao.dynostore.api.model.ScopeMapperImpl
import fr.systao.dynostore.api.model.ScopeRequest
import fr.systao.dynostore.core.domain.entity.ScopeExistsException
import fr.systao.dynostore.core.domain.entity.ScopeNotFoundException
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.service.ScopeService
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
import org.springframework.web.context.WebApplicationContext
import java.util.*


@ApiTest(
    imports=[OIDCConfiguration::class, ScopeMapperImpl::class],
    controllers=[ScopeController::class]
)
class ScopeControllerTest {

    @MockkBean
    private lateinit var service: ScopeService

    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var context: WebApplicationContext

    @Autowired
    private lateinit var oidcConfiguration: OIDCConfiguration

    @BeforeEach
    fun setUp() {
        oidcConfiguration.userIdClaim = "sub"
        mvc = ContextHelper.setupMockMvc(context)
    }

    @Test
    fun `Protected endpoints without Authorization header return 401`() {
        mvc
            .post("/domain/scopes") { }
            .andDo { print() }
            .andExpect { status { isUnauthorized() } }
    }

    @Test
    fun `Protected endpoints with a non Bearer Authorization header return 401`() {
        mvc
            .post("/domain/scopes") {
                headers {
                    this["Authorization"] = "Basic dXNlcjpwYXNzd29yZAo="
                }
            }
            .andDo { print() }
            .andExpect { status { isUnauthorized() } }
    }

    @ParameterizedTest(name = "When {2} is passed should return {1}")
    @MethodSource("testCreateScopeParameters")
    fun `POST scopes endpoint requires proper authentication`(jwt: String?, status: Int, message: String) {
        val scope = Scope("test")
        scope.id = UUID.randomUUID()

        every { service.createScope("test") } returns scope

        val result = postScope(jwt)

        result
            .andDo { print() }
            .andExpect {
                status { isEqualTo(status) }
            }

        if ( status == 200 ) {
            result.andExpect {
                content {
                    jsonPath("$.name") {
                        value("test")
                    }
                    jsonPath("$.id") {
                        scope.id.toString()
                    }
                }
            }
        }
    }

    @Test
    fun `Trying to create a scope that already exists return a 409`() {
        every { service.createScope("test") } throws ScopeExistsException("test")
        val result = postScope(createToken())
        result
            .andDo { print() }
            .andExpect {
                status { isConflict() }
                jsonPath("$.name") {
                    value("test")
                }
                jsonPath("$.code") {
                    value("SCOPE_EXISTS")
                }
            }
    }

    @Test
    fun `Trying to delete a scope that doesn't exist returns a 404`() {
        val jwt = createToken()
        val id = UUID.randomUUID()
        every { service.deleteScope(id) } throws ScopeNotFoundException(id)
        mvc
            .delete("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer $jwt"
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("SCOPE_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to delete an existing scope returns a 204`() {
        val id = UUID.randomUUID()

        every { service.deleteScope(id) } just Runs

        mvc
            .delete("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect { status { isNoContent() } }
    }

    @Test
    fun `Given a number of registered scopes, get scopes returns all of them`()  {
        val scope1 = Scope("test1")
        scope1.id = UUID.randomUUID()

        val scope2 = Scope("test2")
        scope2.id = UUID.randomUUID()

        every { service.loadScopes() } returns  listOf(scope1, scope2)

        mvc
            .get("/domain/scopes") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$", Matchers.hasSize<Int>(2)) {
                        isArray()
                    }
                    jsonPath("$[0].name") {
                        value("test1")
                    }
                    jsonPath("$[1].name") {
                        value("test2")
                    }
                }
            }
    }

    @Test
    fun `Given no scope is registered get scope returns an empty array`()  {
        every { service.loadScopes() } returns  listOf()
        mvc
            .get("/domain/scopes") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$", Matchers.hasSize<Int>(0)) {
                        isArray()
                    }
                }
            }
    }

    @Test
    fun `Given an existing scope of id $id get scope($id) returns it`() {
        val id = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = id
        every { service.loadScope(id) } returns  scope
        mvc
            .get("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$.name") {
                        value("test")
                    }
                    jsonPath("$.id") {
                        value(id.toString())
                    }
                }
            }
    }

    @Test
    fun `Trying to retrieve a non existing scope returns a 404`() {
        val id = UUID.randomUUID()
        every { service.loadScope(id) } throws ScopeNotFoundException(id)
        mvc
            .get("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("SCOPE_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Trying to update a non existing scope returns a 404`() {
        val id = UUID.randomUUID()
        every { service.updateScope(id, "test") } throws ScopeNotFoundException(id)
        mvc
            .put("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test"))
            }
            .andDo { print() }
            .andExpect {
                status { isNotFound() }
                content {
                    jsonPath("$.code") {
                        value("SCOPE_NOT_FOUND")
                    }
                }
            }
    }

    @Test
    fun `Updating an existing scope returns the renamed scope`() {
        val id = UUID.randomUUID()
        val scope = Scope("test2")
        scope.id = id
        every { service.updateScope(id, "test2") } returns  scope
        mvc
            .put("/domain/scopes/$id") {
                headers {
                    this["Authorization"] = "Bearer " + createToken()
                }
                contentType = MediaType.APPLICATION_JSON
                content = mapper.writeValueAsString(ScopeRequest(name = "test2"))
            }
            .andDo { print() }
            .andExpect {
                status { isOk() }
                content {
                    jsonPath("$.name") {
                        value("test2")
                    }
                    jsonPath("$.id") {
                        value(id.toString())
                    }
                }
            }
    }

    private fun postScope(jwt: String?): ResultActionsDsl {
        return mvc.post("/domain/scopes") {
            contentType = MediaType.APPLICATION_JSON
            content = mapper.writeValueAsString(ScopeRequest(name = "test"))
            accept = MediaType.APPLICATION_JSON
            headers {
                this["Authorization"] = "Bearer $jwt"
            }
        }
    }

    companion object {
        val mapper : ObjectMapper = ObjectMapper()

        @JvmStatic
        private fun testCreateScopeParameters() : List<Arguments> {
            return listOf(
                Arguments.of("not.a.valid.jwt", 401, "invalid jwt"),
                Arguments.of(null, 401, "null jwt"),
                Arguments.of(createToken(), 200, "valid jwt"),
            )
        }

    }
}
