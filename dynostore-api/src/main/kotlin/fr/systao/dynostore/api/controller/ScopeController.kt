/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import fr.systao.dynostore.api.model.ScopeMapper
import fr.systao.dynostore.api.model.ScopeRequest
import fr.systao.dynostore.api.model.ScopeResponse
import fr.systao.dynostore.core.domain.entity.service.ScopeService
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(path = ["/domain/scopes"], produces = ["application/json"])
class ScopeController(val scopeService : ScopeService, val scopeMapper : ScopeMapper) {

    @GetMapping
    fun getScopes(authentication: Authentication) : List<ScopeResponse> {
        val scopes = scopeService.loadScopes()
        return scopeMapper.map(scopes)
    }

    @PostMapping
    fun createScope(authentication: Authentication, @RequestBody request : ScopeRequest) : ScopeResponse {
        return scopeMapper.map(scopeService.createScope(request.name))
    }

    @PutMapping(path = ["/{id}"])
    fun updateScope(authentication: Authentication, @PathVariable id: UUID, @RequestBody request : ScopeRequest) : ScopeResponse {
        return scopeMapper.map(scopeService.updateScope(id, request.name))
    }

    @GetMapping(path = ["/{id}"])
    fun getScope(authentication: Authentication, @PathVariable id: UUID) : ScopeResponse? {
        val scope = scopeService.loadScope(id)
        return scopeMapper.map(scope!!)
    }

    @DeleteMapping(path = ["/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteScope(authentication: Authentication, @PathVariable id: UUID) {
        scopeService.deleteScope(id)
    }

}