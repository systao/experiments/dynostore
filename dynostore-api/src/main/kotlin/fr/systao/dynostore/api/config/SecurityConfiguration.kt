/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.config

import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.JWTParser
import fr.systao.dynostore.api.filter.CurlConverterFilterBean
import fr.systao.dynostore.core.security.DynostorePrincipal
import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.streams.toList


@EnableWebSecurity
@Configuration
class SecurityConfiguration(val oidcConfiguration: OIDCConfiguration, @Value("\${dynostore.debug.curl}") val curlConverterEnabled : Boolean) : WebSecurityConfigurerAdapter() {
    companion object : KLogging()

    @Autowired(required = false)
    var converter : CurlConverterFilterBean? = null

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http
            .csrf().disable()
            .httpBasic().disable()

            .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

            .and().exceptionHandling()
                .authenticationEntryPoint(HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))

            .and().authorizeRequests()
                .antMatchers("/domain/**").authenticated()
                .anyRequest().permitAll()

            .and()
                .addFilter(JWTAuthorizationFilter(authenticationManager(), oidcConfiguration))

        if ( curlConverterEnabled ) {
            http.addFilterBefore(converter, JWTAuthorizationFilter::class.java)
        }
    }

}

class JWTAuthorizationFilter(authManager: AuthenticationManager?, private val oidcConfiguration: OIDCConfiguration) : BasicAuthenticationFilter(authManager) {
    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(req: HttpServletRequest, res: HttpServletResponse, chain: FilterChain) {
        val header = req.getHeader(HttpHeaders.AUTHORIZATION)

        if (header == null || !header.startsWith(prefix= "Bearer ", ignoreCase = true)) {
            chain.doFilter(req, res)
            return
        }

        val jwt = header.substringAfter("Bearer ")
        val authentication = authentication(jwt)

        SecurityContextHolder.getContext().authentication = authentication

        chain.doFilter(req, res)
    }

    private fun authentication(jwt: String) : UsernamePasswordAuthenticationToken? {
        //val publicKeys = JWKSet.load(URL(oidcConfiguration.jwksUri))
        //val processor = DefaultJWTProcessor<SimpleSecurityContext>()
        //val claims = processor.process(jwt, null)
        return try {
            val token = JWTParser.parse(jwt)
            val principal = DynostorePrincipalImpl(token.jwtClaimsSet, oidcConfiguration.userIdClaim)
            UsernamePasswordAuthenticationToken(principal, jwt, principal.authorities())
        } catch ( e: Exception ) {
            logger.error("Unable to parse Authorization header", e)
            null
        }
    }
}

class DynostorePrincipalImpl(var claims: JWTClaimsSet, userIdClaim: String) : DynostorePrincipal {
    val id: String = claims.getStringClaim(userIdClaim)

    fun authorities() : Collection<GrantedAuthority> {
        val roles = this.claims.getStringArrayClaim("roles") ?: arrayOf<String>()
        return Arrays.stream(roles)
                .map(this::toAuthority)
                .toList()
    }

    private fun toAuthority(role: String) : GrantedAuthority {
        return SimpleGrantedAuthority("ROLE_" + role.uppercase(Locale.getDefault()))
    }

    override fun id() : String {
        return id
    }
}
