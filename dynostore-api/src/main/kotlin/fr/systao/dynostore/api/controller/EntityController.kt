/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import fr.systao.dynostore.api.model.*
import fr.systao.dynostore.core.domain.entity.model.PropertyKey
import fr.systao.dynostore.core.domain.entity.service.StoreEntityService
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(path = ["/domain/scopes/{scopeId}/entities"], produces = ["application/json"])
class EntityController(val storeEntityService : StoreEntityService, val entityMapper: EntityMapper, val propertyMapper: PropertyMapper) {

    @PostMapping
    fun createEntity(authentication: Authentication, @RequestBody request : EntityRequest, @PathVariable scopeId : UUID) : EntityResponse {
        val entity = entityMapper.map(request)
        val result = storeEntityService.create(request.name, scopeId, entity.properties)
        return entityMapper.map(result)
    }

    @DeleteMapping(path = ["/{id}"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteEntity(authentication: Authentication, @PathVariable scopeId: UUID, @PathVariable id: UUID) {
        storeEntityService.delete(id, scopeId)
    }

    @PatchMapping(path = ["/{id}"])
    fun renameEntity(authentication: Authentication, @PathVariable scopeId: UUID, @PathVariable id: UUID, @RequestBody request: RenameEntityRequest) : EntityResponse {
        return entityMapper.map(storeEntityService.rename(id, scopeId, request.name))
    }

    @PostMapping(path = ["/{id}/properties"])
    fun addProperties(authentication: Authentication, @PathVariable scopeId: UUID, @PathVariable id: UUID, @RequestBody request: List<PropertyDto>) : EntityResponse {
        val property = propertyMapper.map(request)
        return entityMapper.map(storeEntityService.addProperties(id, scopeId, property))
    }

    @PatchMapping(path = ["/{entityId}/properties/{propertyId}"])
    fun renameProperty(authentication: Authentication,
                       propertyKey: PropertyKey,
                       @RequestBody request: RenamePropertyRequest) : PropertyDto {
        return propertyMapper.map(storeEntityService.renameProperty(propertyKey, request.name))
    }

    @DeleteMapping(path = ["/{id}/properties"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteProperties(authentication: Authentication,
                         @PathVariable scopeId: UUID,
                         @PathVariable id: UUID,
                         @RequestBody propertyNames: List<String>) {
        storeEntityService.removeProperties(id, scopeId, propertyNames)
    }

    @GetMapping(path = ["/{id}"])
    fun getEntity(authentication: Authentication, @PathVariable scopeId: UUID, @PathVariable id: UUID) : EntityResponse {
        val entity = storeEntityService.find(scopeId, id)
        return entityMapper.map(entity!!)
    }
}
