/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.model

import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SingleValue
import org.mapstruct.*
import java.math.BigDecimal
import java.util.*


data class PropertyValueDto(val name: String,
                            val value: ValueDto?,
                            val values: List<ValueDto>?)

data class ValueDto(val booleanValue: Boolean?,
                    val stringValue: String?,
                    val integerValue: Int?,
                    val decimalValue: BigDecimal?,
                    val reference: UUID?,
                    val path: String?)

@Mapper(componentModel = "spring", uses = [SingleValueMapper::class], unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface PropertyValueMapper {
    @Mapping(target = "value", ignore = true)
    @Mapping(target = "singleValue", source = "value")
    fun map(propertyValueDto: PropertyValueDto, @Context context: MappingContext): PropertyValue

    @Mapping(source="singleValue", target="value")
    fun map(propertyValue: PropertyValue): PropertyValueDto
}

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface SingleValueMapper {
    @Mapping(target = "reference", ignore = true)
    fun map(value: ValueDto, @Context context: MappingContext): SingleValue

    @Mapping(target = "reference", ignore = true)
    fun map(value: List<ValueDto>, @Context context: MappingContext): List<SingleValue>

    @Mapping(target = "reference", source = "reference.id")
    @Mapping(target = "stringValue", source = "stringValue")
    fun map(value: SingleValue): ValueDto
}

class MappingContext {

    var propertyValue: PropertyValue? = null

    @BeforeMapping
    fun getMappedInstance(@Suppress("UNUSED_PARAMETER") source: ValueDto, @MappingTarget target: SingleValue) {
        target.propertyValue = propertyValue
    }

    @BeforeMapping
    fun getMappedInstance(@Suppress("UNUSED_PARAMETER") source: List<ValueDto>, @MappingTarget target: List<SingleValue>) {
        target.forEach { it.propertyValue = propertyValue }
    }

    @BeforeMapping
    fun register(@Suppress("UNUSED_PARAMETER") source: PropertyValueDto, @MappingTarget target: PropertyValue) {
        propertyValue = target
    }
}
