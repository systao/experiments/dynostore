/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.systao.dynostore.api.error

import fr.systao.dynostore.core.domain.entity.*
import fr.systao.dynostore.core.domain.instance.ForbiddenInstanceAccessException
import fr.systao.dynostore.core.domain.instance.InconsistentInstanceQueryException
import fr.systao.dynostore.core.domain.instance.InstanceNotFoundException
import fr.systao.dynostore.core.domain.instance.PropertyNotFoundException
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ScopeNotFoundException::class)
    fun scopeNotFound(e: ScopeNotFoundException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.NOT_FOUND, "SCOPE_NOT_FOUND", mapOf(Pair("name", e.name)))
    }

    @ExceptionHandler(ScopeExistsException::class)
    fun scopeExists(e: ScopeExistsException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.CONFLICT, "SCOPE_EXISTS", mapOf(Pair("name", e.name)))
    }

    @ExceptionHandler(EntityNotFoundException::class)
    fun entityNotFound(e: EntityNotFoundException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.NOT_FOUND, "ENTITY_NOT_FOUND", mapOf(Pair("entity", mapOf(Pair("name", e.name), Pair("scope", e.id)))))
    }

    @ExceptionHandler(NoSuchPropertyException::class)
    fun propertyNotFound(e: NoSuchPropertyException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.NOT_FOUND, "PROPERTY_NOT_FOUND", mapOf(Pair("property", mapOf(Pair("name", e.name), Pair("entity", e.id)))))
    }

    @ExceptionHandler(PropertyConflictException::class)
    fun propertyNotFound(e: PropertyConflictException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.CONFLICT, "PROPERTY_EXISTS", mapOf(Pair("property", mapOf(Pair("properties", e.properties), Pair("entity", e.entityId)))))
    }

    @ExceptionHandler(InconsistentQueryException::class)
    fun inconsistentModelQuery(e: InconsistentQueryException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.BAD_REQUEST, "INCONSISTENT_QUERY", mapOf(Pair("message", e.message)))
    }

    @ExceptionHandler(InstanceNotFoundException::class)
    fun instanceNotFound(e: InstanceNotFoundException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.NOT_FOUND, "INSTANCE_NOT_FOUND", mapOf(Pair("instance", mapOf(Pair("id", e.instanceId)))))
    }

    @ExceptionHandler(InconsistentInstanceQueryException::class)
    fun inconsistentInstanceQuery(e: InconsistentInstanceQueryException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.BAD_REQUEST, "INCONSISTENT_QUERY", mapOf(Pair("message", e.message)))
    }

    @ExceptionHandler(ForbiddenInstanceAccessException::class)
    fun forbiddenInstanceAccess(e: ForbiddenInstanceAccessException): ResponseEntity<Map<String, Any?>> {
        //return NOT_FOUND if query is inconsistent to prevent security leak. BAD_REQUEST could also be acceptable
        return error(e, HttpStatus.NOT_FOUND, "INSTANCE_NOT_FOUND", mapOf(Pair("instance", e.instanceId)))
    }

    @ExceptionHandler(PropertyNotFoundException::class)
    fun unmappedProperties(e: PropertyNotFoundException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.UNPROCESSABLE_ENTITY, "UNMAPPED_PROPERTIES", mapOf(Pair("properties", e.unmappedProperties)))
    }

    @ExceptionHandler(InvalidParameterException::class)
    fun unmappedProperties(e: InvalidParameterException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.BAD_REQUEST, e.code, mapOf())
    }

    @ExceptionHandler(RuntimeException::class)
    fun uncaught(e: RuntimeException): ResponseEntity<Map<String, Any?>> {
        return error(e, HttpStatus.INTERNAL_SERVER_ERROR, "INTERNAL_ERROR", mapOf())
    }

    private fun error(e: Throwable, status: HttpStatus, code: String, payload: Map<String, Any?>): ResponseEntity<Map<String, Any?>> {
        logger.error("status = $status, code = $code, $payload", e)
        val body = mutableMapOf<String, Any?>(Pair("code", code))
        body.putAll(payload)
        return ResponseEntity
                    .status(status)
                    .body(body)
    }

    companion object : KLogging() { }
}
