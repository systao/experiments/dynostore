/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.model

import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ReportingPolicy
import java.util.*

data class EntityRequest(val name: String, val properties: MutableSet<PropertyDto> = mutableSetOf())

data class RenameEntityRequest(val name: String)

data class EntityResponse(val name: String, val id: UUID, val scopeId: UUID, val fqn: String, val properties: MutableSet<PropertyDto> = mutableSetOf())

@Mapper(componentModel = "spring", uses = [PropertyMapper::class], unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface EntityMapper {

    fun map(request: EntityRequest): StoreEntity

    @Mapping(target="scopeId", source="owner.id")
    @Mapping(target = "fqn", source="fullyQualifiedName")
    fun map(request: StoreEntity): EntityResponse
}
