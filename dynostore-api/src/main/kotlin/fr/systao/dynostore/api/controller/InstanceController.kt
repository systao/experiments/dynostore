/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.controller

import fr.systao.dynostore.api.model.*
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.service.ScopeService
import fr.systao.dynostore.core.domain.entity.service.StoreEntityService
import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SyntheticKey
import fr.systao.dynostore.core.domain.instance.service.InstanceService
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.constraints.Size

@RestController
@RequestMapping(path = ["/domain/scopes/{scopeId}/entities/{entityId}/instances"], produces = ["application/json"])
class InstanceController(val instanceService: InstanceService,
                         val entityService: StoreEntityService,
                         val scopeService: ScopeService,
                         val mapper: InstanceMapper,
                         val propertyValueMapper: PropertyValueMapper) {

    @PostMapping
    fun createInstance(authentication: Authentication,
                       @RequestBody request : InstanceRequest,
                       @PathVariable scopeId : UUID,
                       @PathVariable entityId: UUID) : InstanceResponse {
        val storeEntity =  entityService.find(scopeId, entityId)

        val properties = mutableSetOf<PropertyValue>()

        request.properties?.forEach {
            val property = getPropertyValue(it, storeEntity)
            properties.add(property)
        }

        val scope = scopeService.loadScope(scopeId)

        val instance = instanceService.create(scope, storeEntity!!, properties.toMutableSet())

        return mapper.map(instance!!)
    }

    @GetMapping("/{instanceId}")
    fun getInstance(syntheticKey: SyntheticKey): InstanceResponse {
        return mapper.map(instanceService.find(syntheticKey))
    }

    @DeleteMapping("/{instanceId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteInstance(syntheticKey: SyntheticKey) {
        instanceService.delete(syntheticKey)
    }

    @PutMapping("/{instanceId}")
    fun setProperty(authentication: Authentication,
                    syntheticKey: SyntheticKey,
                    @RequestBody request: PropertyValueDto) : InstanceResponse {
        val scopeId = syntheticKey.scopeId
        val entityId = syntheticKey.entityId
        val storeEntity =  entityService.find(scopeId, entityId)

        val property = getPropertyValue(request, storeEntity)

        val instance = instanceService.setProperty(syntheticKey, request.name, property.getValue())

        return mapper.map(instance)
    }

    @DeleteMapping("/{instanceId}/properties")
    fun removeProperties(authentication: Authentication,
                         syntheticKey: SyntheticKey,
                         @RequestBody @Validated @Size(min = 1) names: Set<String>) : InstanceResponse {
        return mapper.map(instanceService.removeProperties(syntheticKey, names))
    }

    private fun getPropertyValue(request: PropertyValueDto, storeEntity: StoreEntity?): PropertyValue {
        val property = propertyValueMapper.map(request, MappingContext())
        property.property = storeEntity?.getProperty(request.name)
        return property
    }
}