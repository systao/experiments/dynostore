/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.model

import com.fasterxml.jackson.annotation.JsonProperty
import fr.systao.dynostore.core.domain.entity.model.Scope
import org.mapstruct.Mapper
import org.mapstruct.ReportingPolicy
import java.util.*


data class ScopeRequest(@JsonProperty val name: String)

data class ScopeResponse(val name: String, val id: UUID)

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface ScopeMapper {
    fun map(scope: Scope): ScopeResponse
    fun map(scopes: List<Scope>): List<ScopeResponse>
}
