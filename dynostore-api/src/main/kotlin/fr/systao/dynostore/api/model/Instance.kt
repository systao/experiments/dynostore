/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.api.model

import fr.systao.dynostore.core.domain.instance.model.Instance
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.ReportingPolicy
import java.util.*

data class InstanceRequest(val scopeId:  UUID, val properties: List<PropertyValueDto>?)
data class InstanceResponse(val id: UUID, val scopeId: UUID?, val entityId: UUID, val entity: String, val properties: List<PropertyValueDto>?)

@Mapper(componentModel = "spring", uses = [PropertyValueMapper::class], unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface InstanceMapper {

    @Mapping(target = "scopeId", source = "owner.id")
    @Mapping(target = "entityId", source = "storeEntity.id")
    @Mapping(target = "entity", source = "storeEntity.fullyQualifiedName")
    fun map(instance: Instance): InstanceResponse

}