/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.systao.dynostore.api.filter

import mu.KLogging
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.util.ContentCachingRequestWrapper
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
@ConditionalOnProperty(prefix = "dynostore", name = ["debug.curl"], matchIfMissing = false)
@Order(1)
class CurlConverterFilterBean : OncePerRequestFilter() {
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val wrapped = ContentCachingRequestWrapper(request)

        filterChain.doFilter(wrapped, response)

        try {
            val command = convert(wrapped)
            logger.debug(command)
        }
        catch ( e : Exception ) {
            logger.warn("Couldn't convert request to cUrl: ${e.message}");
        }
    }

    private fun convert(request: ContentCachingRequestWrapper): String {
        val url = "\\\n  " + request.requestURL.append(request.queryString ?: "")

        var headers = ""
        for ( header in request.headerNames )  {
            headers += "\\\n  --header '$header: ${request.getHeader(header)}' "
        }

        var body = String(request.contentAsByteArray) //request.reader.lines().collect(Collectors.joining(System.lineSeparator()));
        if ( body.isNotBlank() ) {
            body = "\\\n  --data-raw '$body'"
        }

        return "curl --location --request ${request.method} $url $headers $body"
    }

    companion object : KLogging() { }
}
