/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.model

import assertk.assertAll
import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.domain.entity.NoSuchPropertyException
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class InstanceTest {
    @ParameterizedTest(name = "{3}")
    @MethodSource("testSetPropertyParameters")
    fun testSetProperty(instance: Instance, propertyName: String, exceptException: Boolean, message: String) {
        if ( exceptException ) {
            assertThrows<NoSuchPropertyException> {  instance.setProperty(propertyName, "newValue") }
        }
        else {
            instance.setProperty(propertyName, "newValue")
            assertThat(instance.properties.filter { it.getName() == propertyName }[0].singleValue?.getValue()).isEqualTo(
                "newValue"
            )
        }
    }

    @ParameterizedTest(name = "{3}")
    @MethodSource("testResetPropertyParameters")
    fun testResetProperty(instance: Instance, propertyName: String, expectedPropertyCount: Int, message: String) {
        instance.resetProperty(propertyName)
        assertAll {
            assertThat(instance.properties).hasSize(expectedPropertyCount)
            assertThat(instance.properties.filter { it.getName() == propertyName }).hasSize(0)
        }
    }

    companion object {
        @JvmStatic
        fun testResetPropertyParameters() : Stream<Arguments> {
            val entity = StoreEntity("test")

            val property = Property("testProperty")
            property.primitiveType = PrimitiveType.STRING
            entity.addProperty(property)

            val property2 = Property("testProperty2")
            property.primitiveType = PrimitiveType.STRING
            entity.addProperty(property2)

            return Stream.of(
                Arguments.of(
                    Instance(entity, mutableSetOf(PropertyValue(property), PropertyValue(property2))),
                    "testProperty",
                    1,
                    "trying to reset an existing property actually removes it"),
                Arguments.of(
                    Instance(entity, mutableSetOf(PropertyValue(property), PropertyValue(property2))),
                    "nonExisting",
                    2,
                    "trying to reset an non existing property does nothing")
            )
        }

        @JvmStatic
        fun testSetPropertyParameters() : Stream<Arguments> {
            val entity = StoreEntity("test")

            val property = Property("testProperty")
            property.primitiveType = PrimitiveType.STRING
            entity.addProperty(property)

            return Stream.of(
                Arguments.of(
                    Instance(entity, mutableSetOf(PropertyValue(property))),
                    "testProperty",
                    false,
                    "Calling setProperty changes the value of an existing instance property if present"),
                Arguments.of(
                    Instance(entity, mutableSetOf()),
                    "testProperty",
                    false,
                    "Calling setProperty adds new instance property if none was present"),
                Arguments.of(
                    Instance(entity, mutableSetOf()),
                    "nonExisting",
                    true,
                    "Calling setProperty throws NoSuchPropertyException if the target entity doesn't have any property matching the given name")
            )
        }
    }
}
