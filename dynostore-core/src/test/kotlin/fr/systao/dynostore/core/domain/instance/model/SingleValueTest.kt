/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.model

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import fr.systao.dynostore.core.domain.entity.InconsistentModelException
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.math.BigDecimal

class SingleValueTest {
    @Test
    fun `Trying to get the value of an incompletely configured SingleValue instance throws an InconsistentModelException`() {
        val value = SingleValue()
        assertThrows<InconsistentModelException> { value.getValue() }

        value.propertyValue = PropertyValue(null)
        assertThrows<InconsistentModelException> { value.getValue() }
    }

    @Test
    fun `Trying to get the non set primitive value of a SingleValue instance returns null`() {
        val value = SingleValue()
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setPrimitiveType(PrimitiveType.STRING))
        assertThat(value.getValue()).isNull()
    }

    @Test
    fun `Trying to get a primitive value of type String of a SingleValue instance mapped to a String property returns the configured primitive String value`() {
        testGetValue(PrimitiveType.STRING, SingleValue(stringValue = "single-string-value"), "single-string-value")
    }

    @Test
    fun `Trying to get a primitive value of type Boolean of a SingleValue instance mapped to a Boolean property returns the configured primitive Boolean value`() {
        testGetValue(PrimitiveType.BOOLEAN, SingleValue(booleanValue = true), true)
    }

    @Test
    fun `Trying to get a Decimal value of type Decimal of a SingleValue instance mapped to a Decimal property returns the configured primitive Decimal value`() {
        testGetValue(PrimitiveType.DECIMAL, SingleValue(decimalValue = BigDecimal.valueOf(3.141592)), BigDecimal.valueOf(3.141592))
    }

    @Test
    fun `Trying to get a primitive value of type Integer a SingleValue instance mapped to an Integer property returns the configured primitive Integer value`() {
        testGetValue(PrimitiveType.INTEGER, SingleValue(integerValue = 7), 7)
    }

    @Test
    fun `Trying to get the value of a non primitive SingleValue instance returns the referenced instance`() {
        val entity = StoreEntity("Test")
        val instance = Instance(storeEntity = entity)
        val value = SingleValue(reference = instance)
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setType(entity))
        assertThat(value.getValue()).isEqualTo(instance)
    }

    @Test
    fun `After calling SingleValue#setValue(Integer), getValue returns the correct typed value`() {
        testSetValue(PrimitiveType.INTEGER, SingleValue(integerValue = 7), 5)
    }

    @Test
    fun `After calling SingleValue#setValue(String), getValue returns the correct typed value`() {
        testSetValue(PrimitiveType.STRING, SingleValue(stringValue = "test"), "test2")
    }

    @Test
    fun `After calling SingleValue#setValue(Boolean), getValue returns the correct typed value`() {
        testSetValue(PrimitiveType.BOOLEAN, SingleValue(booleanValue = true), false)
    }

    @Test
    fun `After calling SingleValue#setValue(Decimal), getValue returns the correct typed value`() {
        testSetValue(PrimitiveType.DECIMAL, SingleValue(decimalValue = BigDecimal(3.141592)), BigDecimal(42))
    }

    @Test
    fun `Calling SingleValue#setValue(Any) on a partially formed instance throws InconsistentModelException`() {
        assertThrows<InconsistentModelException> {  SingleValue().setValue("test") }
    }

    @Test
    fun `After calling SingleValue#setValue(REF), getValue returns the correct typed value`() {
        val entity = StoreEntity("Test")
        val instance = Instance(storeEntity = entity)
        val value = SingleValue(reference = instance)
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setType(entity))
        val newValue = Instance(storeEntity = entity)
        value.setValue(newValue)
        assertThat(value.getValue()).isEqualTo(newValue)
    }

    @Test
    fun `Trying to set a non primitive instance of the wrong type throws a ClassCastException`() {
        val entity = StoreEntity("Test")
        val value = SingleValue()
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setType(entity))
        val entity2 = StoreEntity("Test2")
        val newValue = Instance(storeEntity = entity2)
        assertThrows<ClassCastException> { value.setValue(newValue) }
    }

    private fun testGetValue(primitiveType : PrimitiveType, value : SingleValue, expected : Any) {
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setPrimitiveType(primitiveType))
        assertThat(value.getValue()).isEqualTo(expected)
    }

    private fun testSetValue(primitiveType : PrimitiveType, value : SingleValue, newValue : Any) {
        value.propertyValue = PropertyValue(Property("test").setPropertyMultiplicity("1").setPrimitiveType(primitiveType))
        value.setValue(newValue)
        assertThat(value.getValue()).isEqualTo(newValue)
    }
}