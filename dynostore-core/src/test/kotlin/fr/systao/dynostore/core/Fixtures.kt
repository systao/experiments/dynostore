/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core

import fr.systao.dynostore.core.security.DynostorePrincipal
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

class SecurityFixtures {
    companion object {
        const val USER_ID = "user-id"
        fun initializeDynostorePrincipal() {
            SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(
                object : DynostorePrincipal {
                    override fun id() = USER_ID
                },
                "credentials",
                listOf<GrantedAuthority>()
            )
        }

        fun initializeAnonymousUser() {
            SecurityContextHolder.getContext().authentication = AnonymousAuthenticationToken(
                "anonymous",
                "anonymous",
                listOf(SimpleGrantedAuthority("ROLE_ANONYMOUS_ACCESS"))
            )
        }

        fun resetSecurityContext() {
            SecurityContextHolder.clearContext()
        }
    }
}