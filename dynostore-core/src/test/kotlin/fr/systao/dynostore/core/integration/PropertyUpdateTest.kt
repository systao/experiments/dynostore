/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.integration

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.instance.model.Instance
import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SingleValue
import fr.systao.dynostore.core.integration.DynostoreRepositoryIntegrationTest.Companion.WORK_BUDDIES_PROPERTY_NAME
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

@IntegrationTest("POC")
class PropertyUpdateTest {
    @Test
    fun testUpdatePropertyAndChangeValue() {
        val departmentStoreEntity = StoreEntity(DynostoreRepositoryIntegrationTest.DEPARTMENT_ENTITY_NAME)
        val employeeStoreEntity = StoreEntity(DynostoreRepositoryIntegrationTest.EMPLOYEE_ENTITY_NAME)

        val nameProperty = Property(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)
            .setPropertyMultiplicity("1")
            .setPrimitiveType(PrimitiveType.STRING)

        val departmentProperty = Property(DynostoreRepositoryIntegrationTest.DEPARTMENT_PROPERTY_NAME)
            .setType(departmentStoreEntity)

        val buddiesProperty = Property(DynostoreRepositoryIntegrationTest.WORK_BUDDIES_PROPERTY_NAME)
            .setType(employeeStoreEntity)
            .unbind()

        employeeStoreEntity
            .addProperty(nameProperty)
            .addProperty(departmentProperty)
            .addProperty(buddiesProperty)

        val jane = PropertyValue(nameProperty).setValue("Jane")
        val employeeWorkBuddiesPropertyValue = PropertyValue(buddiesProperty)
        val buddiesSet = mutableSetOf(
            SingleValue(reference = Instance(employeeStoreEntity).addProperty(jane)),
            SingleValue(reference = Instance(employeeStoreEntity).addProperty(PropertyValue(nameProperty).setValue("Jeff")))
        )
        employeeWorkBuddiesPropertyValue.setMultiValue(buddiesSet)

        val instance = Instance(employeeStoreEntity)
            .addProperty(PropertyValue(nameProperty).setValue("John Doe"))

        assertThat(instance.getProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)).isEqualTo("John Doe")

        nameProperty.setPrimitiveType(PrimitiveType.BOOLEAN)
        assertThat(instance.getProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)).isNull()

        instance.setProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME, true)
        assertThat(instance.getProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)).isEqualTo(true)

        assertThat(instance.properties).hasSize(1)

        nameProperty.setPrimitiveType(PrimitiveType.STRING)
        assertThat(instance.getProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)).isEqualTo("John Doe")

        assertThat(instance.properties).hasSize(1)

        instance.setProperty(WORK_BUDDIES_PROPERTY_NAME, mutableSetOf(
            Instance(employeeStoreEntity).addProperty(jane),
            Instance(employeeStoreEntity).addProperty(PropertyValue(nameProperty).setValue("Jeff"))
        ))
        val buddies = instance.getProperty(WORK_BUDDIES_PROPERTY_NAME)
        Assertions.assertThat(buddies).isNotNull
        Assertions.assertThat(buddies as Set<*>).hasSize(2)

        val copy = mutableSetOf(buddies.first())
        instance.setProperty(WORK_BUDDIES_PROPERTY_NAME, copy)
        Assertions.assertThat(instance.getProperty(WORK_BUDDIES_PROPERTY_NAME) as Set<*>).hasSize(1)

        instance.resetProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)
        assertThat(instance.getProperty(DynostoreRepositoryIntegrationTest.NAME_PROPERTY_NAME)).isNull()
        assertThat(instance.properties).hasSize(1)

    }
}