/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core

import assertk.assertThat
import assertk.assertions.hasValue
import assertk.assertions.isEmpty
import org.junit.jupiter.api.Test

class DynostoreCoreConfigurationTest {
    @Test
    fun `Given a DynostorePrincipal getCurrentAuditor return the principal id`() {
        SecurityFixtures.initializeDynostorePrincipal()
        assertThat(EntityAuditorAware().currentAuditor).hasValue("user-id")
    }

    @Test
    fun `Given a non DynostorePrincipal getCurrentAuditor return an empty optional`() {
        SecurityFixtures.initializeAnonymousUser()
        assertThat(EntityAuditorAware().currentAuditor).isEmpty()
    }

    @Test
    fun `Given non security context getCurrentAuditor return an empty opitonal`() {
        SecurityFixtures.resetSecurityContext()
        assertThat(EntityAuditorAware().currentAuditor).isEmpty()
    }
}
