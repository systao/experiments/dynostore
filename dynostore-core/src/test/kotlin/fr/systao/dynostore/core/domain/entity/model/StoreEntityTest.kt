/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.model

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.domain.entity.NoSuchPropertyException
import fr.systao.dynostore.core.domain.entity.PropertyConflictException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class StoreEntityTest {
    @ParameterizedTest
    @MethodSource("testGetFullyQualifiedNameParameters")
    fun testGetFullyQualifiedName(entity: StoreEntity, expectedResult: String) {
        assertThat(entity.getFullyQualifiedName()).isEqualTo(expectedResult)
    }

    @Test
    fun testSetOwner() {
        val entity = StoreEntity("test")
        val scope = Scope("pck")
        entity.setOwner(scope)

        assertThat(scope.entities).hasSize(1)
    }

    @Test
    fun changeOwner() {
        val entity = StoreEntity("test")
        val scope = Scope("pck")
        entity.setOwner(scope)

        val newScope = Scope("pck2")
        entity.setOwner(newScope)

        assertThat(scope.entities).hasSize(0)
        assertThat(newScope.entities).hasSize(1)
    }

    @Test
    fun testSetOwnerToNull() {
        val entity = StoreEntity("test")
        val scope = Scope("pck")
        entity.setOwner(scope)

        entity.setOwner(null)
        assertThat(scope.entities).hasSize(0)
    }

    @ParameterizedTest
    @MethodSource("testHasPropertyParameters")
    fun testHasProperty_ByString(entity: StoreEntity, propertyName: String, expectedResult: Boolean) {
        assertThat(entity.hasProperty(propertyName)).isEqualTo(expectedResult)
    }

    @ParameterizedTest
    @MethodSource("testHasPropertyParameters")
    fun testHasProperty_ByProperty(entity: StoreEntity, propertyName: String, expectedResult: Boolean) {
        assertThat(entity.hasProperty(Property(propertyName))).isEqualTo(expectedResult)
    }

    @ParameterizedTest
    @MethodSource("testAddPropertyParameters")
    fun testAddProperties(entity: StoreEntity, property: Property, expectedCount: Int) {
        entity.addProperty(property)
        assertThat(entity.properties).hasSize(expectedCount)
    }

    @Test
    fun `Trying to add an existing property should throw a PropertyConflictException`() {
        val entity = StoreEntity("test1")
        entity.properties = mutableSetOf(Property("test"), Property("test3"))
        assertThrows<PropertyConflictException> {  entity.addProperty(Property("test")) }
    }

    @Test
    fun testRemoveProperties() {
        val entity = StoreEntity("test1")
        entity.properties = mutableSetOf(Property("test"), Property("test3"))
        entity.removeProperty("test")
        assertThat(entity.properties).hasSize(1)
    }

    @Test
    fun `Trying to remove a non existing property should throw a NoSuchPropertyException`() {
        val entity = StoreEntity("test1")
        entity.properties = mutableSetOf(Property("test"), Property("test3"))
        assertThrows<NoSuchPropertyException> {  entity.removeProperty("test2") }
    }

    @Test
    fun testGetProperty() {
        val entity = StoreEntity("test1")
        val property = Property("test")
        entity.properties = mutableSetOf(property, Property("test3"))
        assertThat(entity.getProperty("test")).isEqualTo(property)
    }

    @Test
    fun `Trying to retrieve a non existing property should throw a NoSuchPropertyException`() {
        val entity = StoreEntity("test1")
        entity.properties = mutableSetOf(Property("test"), Property("test3"))
        assertThrows<NoSuchPropertyException> {  entity.getProperty("test2") }
    }

    @ParameterizedTest
    @MethodSource("testNameParameters")
    fun testName(entityName: String?, expectedResult: String) {
        assertThat(StoreEntity(entityName).resolveName()).isEqualTo(expectedResult)
    }

    companion object {
        @JvmStatic
        fun testGetFullyQualifiedNameParameters() : Stream<Arguments> {
            val entity = StoreEntity("test")
            entity.setOwner(Scope("pck"))
            return Stream.of(
                Arguments.of(StoreEntity("test"), "test"),
                Arguments.of(entity, "pck.test")
            )
        }

        @JvmStatic
        fun testHasPropertyParameters() : Stream<Arguments> {
            val entity1 = StoreEntity("test1")
            entity1.properties = mutableSetOf(Property("test"), Property("test3"))

            val entity2 = StoreEntity("test2")

            return Stream.of(
                Arguments.of(entity1, "test", true),
                Arguments.of(entity1, "test2", false),
                Arguments.of(entity1, "test3", true),
                Arguments.of(entity2, "test", false)
            )
        }

        @JvmStatic
        fun testAddPropertyParameters() : Stream<Arguments> {
            val entity1 = StoreEntity("test1")
            entity1.properties = mutableSetOf(Property("test"), Property("test3"))

            return Stream.of(
                Arguments.of(entity1, Property("test2"), 3),
                Arguments.of(StoreEntity("test2"), Property("test1"), 1)
            )
        }

        @JvmStatic
        fun testNameParameters() : Stream<Arguments> {
            return Stream.of(
                Arguments.of(null, "Nil"),
                Arguments.of("test", "test")
            )
        }
    }
}