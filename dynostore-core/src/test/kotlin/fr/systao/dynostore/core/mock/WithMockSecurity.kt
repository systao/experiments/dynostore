/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.mock

import fr.systao.dynostore.core.security.DynostorePrincipal
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.test.context.support.WithSecurityContext
import org.springframework.security.test.context.support.WithSecurityContextFactory
import java.util.*
import java.util.stream.Collectors


class WithMockPrincipalSecurityContextFactory : WithSecurityContextFactory<WithMockPrincipal> {
    override fun createSecurityContext(mockPrincipal: WithMockPrincipal): SecurityContext {
        val context = SecurityContextHolder.getContext()

        val authorities: Collection<GrantedAuthority> = Arrays.stream(mockPrincipal.roles)
            .map { role: String? -> SimpleGrantedAuthority(role) }
            .collect(Collectors.toList())

        val principal = object : DynostorePrincipal {
            override fun id() = mockPrincipal.id
        }

        val auth: Authentication = UsernamePasswordAuthenticationToken(principal, null, authorities)
        context.authentication = auth

        return context
    }
}

@Retention(AnnotationRetention.RUNTIME)
@WithSecurityContext(factory = WithMockPrincipalSecurityContextFactory::class)
annotation class WithMockPrincipal(val id: String, val roles: Array<String> = [])