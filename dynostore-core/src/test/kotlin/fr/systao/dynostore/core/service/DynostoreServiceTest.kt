/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.service

import assertk.assertThat
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.SecurityFixtures
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.security.access.AccessDeniedException

class DynostoreServiceTest {

    @Test
    fun `Given the security context contains a DynostorePrincipal, DynostoreService#ifValidAuthentication should call the ifValid function parameter`() {
        SecurityFixtures.initializeDynostorePrincipal()
        val service = object : DynostoreService() {
            fun whenValid() : String {
                return ifValidAuthentication{ p -> "user ${p.id()}" }
            }
        }
        assertThat(service.whenValid()).isEqualTo("user ${SecurityFixtures.USER_ID}")
    }

    @Test
    fun `Given the principal is not a DynostorePrincipal, DynostoreService#ifValidAuthentication should throw an AccessDeniedException`() {
        SecurityFixtures.initializeAnonymousUser()
        val service = object : DynostoreService() {
            fun whenValid() : String {
                return ifValidAuthentication{ p -> "user ${p.id()}" }
            }
        }
        assertThrows<AccessDeniedException> {  service.whenValid() }
    }

    @Test
    fun `Given no security context, DynostoreService#ifValidAuthentication should throw an AccessDeniedException`() {
        SecurityFixtures.resetSecurityContext()
        val service = object : DynostoreService() {
            fun whenValid() : String {
                return ifValidAuthentication{ p -> "user ${p.id()}" }
            }
        }
        assertThrows<AccessDeniedException> {  service.whenValid() }
    }
}