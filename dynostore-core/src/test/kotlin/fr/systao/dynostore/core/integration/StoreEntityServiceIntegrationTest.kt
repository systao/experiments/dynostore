/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.integration

import fr.systao.dynostore.core.SecurityFixtures
import fr.systao.dynostore.core.domain.entity.ScopeNotFoundException
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.repository.ScopeRepository
import fr.systao.dynostore.core.domain.entity.repository.StoreEntityRepository
import fr.systao.dynostore.core.domain.entity.service.StoreEntityService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.access.AccessDeniedException
import org.springframework.test.context.ActiveProfiles
import java.util.*
import javax.transaction.Transactional


@SpringBootTest
@IntegrationTest("POC")
@ActiveProfiles("test")
@Transactional
class StoreEntityServiceIntegrationTest {
    @Autowired
    private lateinit var  service: StoreEntityService

    @Autowired
    private lateinit var  storeEntityRepository: StoreEntityRepository

    @Autowired
    private lateinit var  scopeRepository: ScopeRepository

    @BeforeEach
    fun setUp() {
        SecurityFixtures.initializeDynostorePrincipal()
    }

    @Test
    fun createEntityFromPackage() {
        val rootScope = Scope(PACKAGE_NAME)
        val entity = service.create(ENTITY_NAME, rootScope, mutableSetOf(Property(PROPERTY_NAME).setPrimitiveType(PrimitiveType.STRING)))
        assertEntityCreated(entity)
    }

    @Test
    fun createEntityFromId() {
        val rootScope = Scope(PACKAGE_NAME)
        rootScope.createdBy = SecurityFixtures.USER_ID
        val ownerId = scopeRepository.save(rootScope).id
        val entity = service.create(ENTITY_NAME, ownerId!!, mutableSetOf(Property(PROPERTY_NAME).setPrimitiveType(PrimitiveType.STRING)))
        assertEntityCreated(entity)
    }

    @Test
    fun testCreateEntityPackageNotFound() {
        val id = UUID.randomUUID()
        Assertions.assertThatThrownBy { service.create(ENTITY_NAME, id, mutableSetOf()) }
                .isInstanceOf(ScopeNotFoundException::class.java)
                .hasFieldOrPropertyWithValue("id", id)
    }

    @Test
    fun `anonymous users should not be able to create entities`() {
        SecurityFixtures.initializeAnonymousUser()
        assertThrows<AccessDeniedException> {
            service.create(
                ENTITY_NAME,
                Scope(PACKAGE_NAME),
                mutableSetOf()
            )
        }
    }

    private fun assertEntityCreated(createdStoreEntity: StoreEntity?) {
        var entity = createdStoreEntity
        val id = entity?.id
        Assertions.assertThat(id).isNotNull

        val optional = storeEntityRepository.findById(id!!)
        Assertions.assertThat(optional).isNotEmpty
        entity = optional.get()

        val properties = entity.properties
        Assertions.assertThat(properties).hasSize(1)
        Assertions.assertThat(properties.stream().findFirst().get().name).isEqualTo(PROPERTY_NAME)

        val pck = scopeRepository.findByNameAndCreatedBy(PACKAGE_NAME, "user-id")
        val packageId = pck?.id
        Assertions.assertThat(packageId).isNotNull

        storeEntityRepository.findAll().forEach {
            println(it?.id.toString()+ " ===> " + it?.getOwner()?.id.toString())
        }
        scopeRepository.findAll().forEach {
            println(it?.id.toString()+ " ===> " + it?.entities?.first()?.id.toString())
        }

        val entities = storeEntityRepository.findByOwnerIdAndCreatedBy(packageId!!, "user-id")
        Assertions.assertThat(entities.count()).isEqualTo(1)
    }

    companion object {
        const val PACKAGE_NAME: String = "root"
        const val ENTITY_NAME: String = "Employee"
        const val PROPERTY_NAME: String = "name"
    }
}
