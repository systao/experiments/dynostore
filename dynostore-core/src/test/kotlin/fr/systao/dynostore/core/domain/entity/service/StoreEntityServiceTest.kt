/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.service

import assertk.assertAll
import assertk.assertThat
import assertk.assertions.containsAll
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.SecurityFixtures
import fr.systao.dynostore.core.domain.entity.*
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.PropertyKey
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.mock.WithMockPrincipal
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*
import kotlin.streams.toList

class StoreEntityServiceTest {

    @BeforeEach
    fun setUp() {
        SecurityFixtures.initializeDynostorePrincipal()
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `Given an Entity e of name 'test' in a given or null scope s, calling StoreEntityService#find(name, s) returns e`() {
        val service = StoreEntityService(mockk(), mockk())
        val entity = StoreEntity("test")

        every { service.storeEntityRepository.findByOwnerIdAndNameAndCreatedBy(any(), "test", any()) } returns entity

        val result = service.find("test", UUID.randomUUID())
        assertThat(result).isEqualTo(entity)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    @WithMockPrincipal(id = "user-id", roles = [])
    fun `Given a String name and any() Scope, calling StoreEntityService#find(name, s) throws EntityNotFoundException if entity no entity of name 'name' exists in scope s`() {
        val service = StoreEntityService(mockk(), mockk())
        every { service.storeEntityRepository.findByOwnerIdAndNameAndCreatedBy(any(), "test", any()) } throws EntityNotFoundException("test", null)
        assertThrows<EntityNotFoundException> { service.find("test", UUID.randomUUID()) }
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `StoreEntityService#delete delegates to StoreEntityRepository#delete`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())
        val entity = StoreEntity("test")
        val id = UUID.randomUUID()

        entity.id = id
        every { service.storeEntityRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns entity

        service.delete(id, null)
        verify { service.storeEntityRepository.deleteById(id) }
    }

    @Test
    fun `Trying to delete a not existing Entity throws an EntityNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())
        val entity = StoreEntity("test")
        val id = UUID.randomUUID()

        entity.id = id
        every { service.storeEntityRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } throws EntityNotFoundException(id)

        assertAll {
            assertThrows<EntityNotFoundException> { service.delete(id, null) }
            verify(exactly = 0) { service.storeEntityRepository.deleteById(id) }
        }
    }

    @Test
    fun `Trying to delete a Entity that doesn't belong to the correct scope throws an InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entity = StoreEntity("test")
        val id = UUID.randomUUID()

        val scope = Scope("test")
        scope.id = UUID.randomUUID()

        entity.setOwner(scope)
        entity.id = id
        every { service.storeEntityRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns entity

        assertAll {
            assertThrows<InconsistentQueryException> { service.delete(id, UUID.randomUUID()) }
            verify(exactly = 0) { service.storeEntityRepository.deleteById(id) }
        }
    }

    @Test
    fun `StoreEntityService#findByScope delegates to repository#findByOwnerIdAndCreatedBy`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())
        val scopeId = UUID.randomUUID()
        every { service.storeEntityRepository.findByOwnerIdAndCreatedBy(scopeId, SecurityFixtures.USER_ID) } returns listOf(StoreEntity("test1"), StoreEntity("test2")).stream()
        val result = service.findByScope(scopeId)?.toList()!!
        assertAll {
            assertThat(result).hasSize(2)
            assertThat(result.stream().map(StoreEntity::name)?.toList()!!).containsAll("test1", "test2")
        }
    }

    @Test
    fun `StoreEntityService#find(uuid, uuid) delegates to StoreEntityRepository#findBy`() {
        val service = StoreEntityService(mockk(), mockk())
        val entity = StoreEntity("test")
        val scopeId = UUID.randomUUID()
        val entityId = UUID.randomUUID()
        every { service.storeEntityRepository.findByOwnerIdAndIdAndCreatedBy(scopeId, entityId, SecurityFixtures.USER_ID) } returns entity
        assertThat(service.find(scopeId = scopeId, id = entityId)).isEqualTo(entity)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    @WithMockPrincipal(id = "user-id", roles = [])
    fun `StoreEntityService#find(uuid, uuid) throws EntityNotFoundException if no entity is found`() {
        val service = StoreEntityService(mockk(), mockk())
        every { service.storeEntityRepository.findByOwnerIdAndIdAndCreatedBy(any(), any(), any()) } throws EntityNotFoundException("test", null)
        assertThrows<EntityNotFoundException> { service.find(UUID.randomUUID(), UUID.randomUUID()) }
    }

    @Test
    fun `StoreEntityService#create delegates to StoreEntityRepository#save`() {
        val service = StoreEntityService(mockk(), mockk())

        val properties = mutableSetOf<Property>()
        val entity = StoreEntity("mock")

        val argument = slot<StoreEntity>()
        every { service.storeEntityRepository.save(capture(argument)) } returns entity

        val scope = Scope("test")
        service.create("test", scope, properties)

        assertAll {
            verify { service.storeEntityRepository.save(any()) }
            assertThat(argument.captured.name).isEqualTo("test")
            assertThat(argument.captured.properties).isEqualTo(properties)
        }
    }

    @Test
    fun `Given an existing entity StoreEntityService#rename correctly renames the entity and delegates to repository`() {
        val service = StoreEntityService(mockk(), mockk())

        val entity = StoreEntity("mock")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId

        entity.setOwner(scope)

        val argument = slot<StoreEntity>()
        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(capture(argument)) } returns entity

        service.rename(entityId, scopeId, "test2")

        assertAll {
            verify { service.storeEntityRepository.save(any()) }
            assertThat(argument.captured.name).isEqualTo("test2")
        }
    }

    @Test
    fun `Trying to rename an entity in a scope that doesn't match the scopeId parameter throws an InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entity = StoreEntity("mock")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity

        assertAll {
            assertThrows<InconsistentQueryException> {  service.rename(entityId, UUID.randomUUID(), "test2") }
        }
    }

    @Test
    fun `Scope parameter passed to StoreEntityService#create must belong to the current user`() {
        val service = StoreEntityService(mockk(), mockk(relaxed = true))
        every { service.scopeRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertThrows<ScopeNotFoundException> { service.create("test", UUID.randomUUID(), mutableSetOf()) }
    }

    @Test
    fun `When calling StoreEntityService#create with a scope uuid, the scope is correctly retrieved and set`() {
        val service = StoreEntityService(mockk(), mockk(relaxed = true))
        val properties = mutableSetOf<Property>()

        val scope = Scope()
        scope.id = UUID.randomUUID()

        val entity = StoreEntity("mock")
        val argument = slot<StoreEntity>()
        every { service.storeEntityRepository.save(capture(argument)) } returns entity
        every { service.scopeRepository.findByIdAndCreatedBy(scope.id!!, SecurityFixtures.USER_ID) } returns scope

        service.create("test", scope.id!!, properties)

        val result = scope.entities.first()!!

        assertAll {
            verify { service.storeEntityRepository.save(any()) }
            assertThat(argument.captured.getOwner()).isEqualTo(scope)
            assertThat(result.name).isEqualTo("test")
            assertThat(result.getOwner()).isEqualTo(scope)
        }
    }

    @Test
    fun `Trying to create an Entity in a non existing scope referenced by uuid throws a ScopeNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())
        val id = UUID.randomUUID()

        every { service.scopeRepository.findByIdAndCreatedBy(id, "user-id") } throws ScopeNotFoundException(id)

        assertAll {
            val exception = assertThrows<ScopeNotFoundException> { service.create("name", id, mutableSetOf()) }
            assertThat(exception.id).isEqualTo(id)
        }
    }

    @Test
    fun `Trying to add properties to an entity in a scope that doesn't match the scopeId parameter throws an InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity

        val property = Property("test2")

        assertAll {
            assertThrows<InconsistentQueryException> {  service.addProperties(entityId, UUID.randomUUID(), listOf(property)) }
            verify(exactly = 0) { service.storeEntityRepository.save(any()) }
        }
    }

    @Test
    fun `Given an existing entity, calling addProperties correctly adds the passed properties to the entity`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        val property = Property("test2")
        val result = service.addProperties(entityId, scopeId, listOf(property))

        assertAll {
            assertThat(result).isEqualTo(entity)
            assertThat(result.properties).hasSize(2)
            assertThat(result.properties.stream().map(Property::name).toList()).containsAll("test1", "test2")
            verify(exactly = 1) { service.storeEntityRepository.save(entity) }
        }
    }

    @Test
    fun `Trying add an homonym property to an entity throws a PropertyConflictException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        val property = Property("test1")

        assertThrows<PropertyConflictException> { service.addProperties(entityId, scopeId, listOf(property)) }
    }

    @Test
    fun `Trying to add simultaneously two homonym properties to an entity throws a PropertyConflictException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        val property1 = Property("test2")
        val property2 = Property("test2")

        assertThrows<InconsistentQueryException> { service.addProperties(entityId, scopeId, listOf(property1, property2)) }
    }

    @Test
    fun `Given an existing entity, calling removeProperties correctly removes the referenced properties from the entity`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        val result = service.removeProperties(entityId, scopeId, listOf("test2"))

        assertAll {
            assertThat(result).isEqualTo(entity)
            assertThat(result.properties).hasSize(2)
            assertThat(result.properties.stream().map(Property::name).toList()).containsAll("test1", "test3")
            verify(exactly = 1) { service.storeEntityRepository.save(entity) }
        }
    }

    @Test
    fun `Given an existing entity, trying to remove a non existing property throws an InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        assertThrows<InconsistentQueryException> { service.removeProperties(entityId, scopeId, listOf("test4")) }
    }

    @Test
    fun `Given an existing entity, trying to remove simultaneously the same property throws an InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(any()) } returns entity

        assertThrows<InconsistentQueryException> { service.removeProperties(entityId, scopeId, listOf("test2", "test2")) }
    }

    @Test
    fun `Trying to remove a property to a non existing entity or an entity not owned by the current user throws an EntityNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk(relaxed = true))
        every { service.storeEntityRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertThrows<EntityNotFoundException> { service.removeProperties(entityId = UUID.randomUUID(), scopeId = UUID.randomUUID(), listOf()) }
    }

    @Test
    fun `Trying to add a property to a non existing entity or an entity not owned by the current user throws an EntityNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk(relaxed = true))
        every { service.storeEntityRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertThrows<EntityNotFoundException> { service.addProperties(entityId = UUID.randomUUID(), scopeId = UUID.randomUUID(), listOf()) }
    }

    @Test
    fun `Trying to rename a non existing entity or an entity not owned by the current user throws an EntityNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk(relaxed = true))
        every { service.storeEntityRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertThrows<EntityNotFoundException> { service.rename(entityId = UUID.randomUUID(), scopeId = UUID.randomUUID(), "newName") }
    }

    @Test
    fun `Trying to rename a property from an entity not owned by the current user throws an EntityNotFoundException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk(relaxed = true))
        every { service.storeEntityRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertThrows<EntityNotFoundException> {
            service.renameProperty(PropertyKey(entityId = UUID.randomUUID(), scopeId = UUID.randomUUID(), propertyId = UUID.randomUUID()), name = "newName")
        }
    }

    @Test
    fun `Given an entity e, trying to remove a non existing property from e throws a NoSuchPropertyException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId
        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity

        assertThrows<NoSuchPropertyException> {
            service.renameProperty(PropertyKey(entityId = entityId, scopeId = scopeId, propertyId = UUID.randomUUID()), name = "newName")
        }
    }

    @Test
    fun `Given an entity e, trying to remove a property from e that doesn't belong to the given scope  throws a InconsistentQueryException`() {
        val service = StoreEntityService(mockk(relaxed = true), mockk())

        val entityId = UUID.randomUUID()
        val entity = StoreEntity("test")
        entity.id = entityId

        val scopeId = UUID.randomUUID()
        val scope = Scope("test")
        scope.id = scopeId

        entity.setOwner(scope)

        entity.properties = mutableSetOf(Property("test1"), Property("test2"), Property("test3"))

        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity

        assertThrows<InconsistentQueryException> {
            service.renameProperty(PropertyKey(entityId = entityId, scopeId = UUID.randomUUID(), propertyId = UUID.randomUUID()), name = "newName")
        }
    }

    @Test
    fun `Given an existing property StoreEntityService#renameProperty correctly renames the entity and delegates to repository#save`() {
        val service = StoreEntityService(mockk(), mockk())

        val entity = StoreEntity("mock")
        val entityId = UUID.randomUUID()
        entity.id = entityId

        val scope = Scope("test")
        val scopeId = UUID.randomUUID()
        scope.id = scopeId

        entity.setOwner(scope)

        val propertyId = UUID.randomUUID()
        val property = Property("test1")
        property.id = propertyId

        entity.properties = mutableSetOf(property, Property("test2"), Property("test3"))

        val argument = slot<StoreEntity>()
        every { service.storeEntityRepository.findByIdAndCreatedBy(entityId, SecurityFixtures.USER_ID) } returns entity
        every { service.storeEntityRepository.save(capture(argument)) } returns entity

        service.renameProperty(PropertyKey(scopeId, entityId, propertyId),"newName")

        assertAll {
            verify { service.storeEntityRepository.save(entity) }
            assertThat(argument.captured.properties.filter { it.id == propertyId }[0].name).isEqualTo("newName")
        }
    }
}
