/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.integration

import fr.systao.dynostore.core.SecurityFixtures
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.repository.ScopeRepository
import fr.systao.dynostore.core.domain.entity.repository.StoreEntityRepository
import fr.systao.dynostore.core.domain.instance.model.Instance
import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SingleValue
import fr.systao.dynostore.core.domain.instance.repository.InstanceRepository
import mu.KLogging
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.util.stream.Collectors
import javax.transaction.Transactional

@SpringBootTest
@IntegrationTest("POC")
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation::class)
@Transactional
class DynostoreRepositoryIntegrationTest {

    @Autowired
    private lateinit var storeEntityRepository: StoreEntityRepository

    @Autowired
    private lateinit var scopeRepository: ScopeRepository

    @Autowired
    private lateinit var instanceRepository: InstanceRepository

    @BeforeEach
    fun setUp() {
        SecurityFixtures.initializeDynostorePrincipal()

        val rootScope = Scope(PACKAGE_NAME)
        rootScope.createdBy = SecurityFixtures.USER_ID
        val departmentStoreEntity = StoreEntity(DEPARTMENT_ENTITY_NAME)
        val employeeStoreEntity = StoreEntity(EMPLOYEE_ENTITY_NAME)

        employeeStoreEntity
                .addProperty(Property(NAME_PROPERTY_NAME).setPropertyMultiplicity("1").setPrimitiveType(PrimitiveType.STRING))
                .addProperty(Property(DEPARTMENT_PROPERTY_NAME).setType(departmentStoreEntity))
                .addProperty(Property(WORK_BUDDIES_PROPERTY_NAME).setType(employeeStoreEntity).unbind())

        rootScope
                .add(employeeStoreEntity)
                .add(departmentStoreEntity)

        scopeRepository.save(rootScope)
    }

    @Test
    @Order(1)
    fun testSimpleModelCreation() {
        val pck = scopeRepository.findByNameAndCreatedBy(PACKAGE_NAME, "user-id")

        logger.error { "Read Package $pck?.name from repository. Has $pck?.entities?.size entities" }
        Assertions.assertThat(pck?.entities).hasSize(2)

        val departmentEntity = pck?.entities!!
                .stream()
                .filter { e: StoreEntity? -> e?.name == DEPARTMENT_ENTITY_NAME }
                .findFirst().orElseThrow()

        val employeeEntity = pck.entities
                .stream()
                .filter { e: StoreEntity? -> e?.name == EMPLOYEE_ENTITY_NAME }
                .findFirst().orElseThrow()

        val properties = employeeEntity?.properties
        Assertions.assertThat(properties)?.hasSize(3)

        val nameProperty = getProperty(properties, NAME_PROPERTY_NAME)
        Assertions.assertThat(nameProperty?.multiplicity).isEqualTo("1")

        val departmentProperty = getProperty(properties, DEPARTMENT_PROPERTY_NAME)
        Assertions.assertThat(departmentProperty?.multiplicity).isEqualTo("1")
        Assertions.assertThat(departmentProperty?.storeEntity).isEqualTo(departmentEntity)

        val buddiesProperty = getProperty(properties, WORK_BUDDIES_PROPERTY_NAME)
        Assertions.assertThat(buddiesProperty?.multiplicity).isEqualTo("*")

        val entities = storeEntityRepository.findByOwnerIdAndCreatedBy(pck.id!!, SecurityFixtures.USER_ID)
        Assertions.assertThat(entities).hasSize(2)

        val optionalProperty = employeeEntity?.getProperty(DEPARTMENT_PROPERTY_NAME)
        Assertions.assertThat(optionalProperty)?.isEqualTo(departmentProperty)
    }

    @Test
    @Order(2)
    fun testCreateInstance() {
        val pck = scopeRepository.findByNameAndCreatedBy(PACKAGE_NAME, "user-id")

        val entity = pck?.entities!!
                .stream()
                .filter { e: StoreEntity? -> e?.name == EMPLOYEE_ENTITY_NAME }
                .findFirst().orElseThrow()
        val property = entity?.getProperty(NAME_PROPERTY_NAME)
        val buddiesProperty = entity?.getProperty(WORK_BUDDIES_PROPERTY_NAME)
        val owner = Scope("$PACKAGE_NAME.instances")

        val jane = PropertyValue(property).setValue("Jane")
        val employeeWorkBuddiesPropertyValue = PropertyValue(buddiesProperty)
        val buddiesSet = mutableSetOf(
            SingleValue(reference = Instance(entity!!).addProperty(jane).move(owner)),
            SingleValue(reference = Instance(entity).addProperty(PropertyValue(property).setValue("Jeff")).move(owner)))
        employeeWorkBuddiesPropertyValue.setMultiValue(buddiesSet)

        var instance = Instance(entity)
                .move(owner)
                .addProperty(PropertyValue(property).setValue("John Doe"))
                .addProperty(employeeWorkBuddiesPropertyValue)
        val testInstanceId = instanceRepository.save(instance).id

        val stream = instanceRepository.findByStoreEntityId(entity.id!!)
        val instances = stream.collect(Collectors.toList())
        Assertions.assertThat(instances).hasSize(3)

        instance = instances.stream().filter { it?.id == testInstanceId }.findFirst().get()
        Assertions.assertThat(instance.storeEntity?.name).isEqualTo(EMPLOYEE_ENTITY_NAME)
        Assertions.assertThat(instance.properties).hasSize(2)

        val name = instance.getProperty(NAME_PROPERTY_NAME)
        Assertions.assertThat(name).isEqualTo("John Doe")
        Assertions.assertThat(instance.getProperty(DEPARTMENT_PROPERTY_NAME)).isNull()

        val buddies = instance.getProperty(WORK_BUDDIES_PROPERTY_NAME)
        Assertions.assertThat(buddies).isNotNull
        Assertions.assertThat(buddies as Set<*>).hasSize(2)
    }

    private fun getProperty(
        properties: Set<Property?>?,
        propertyName: String
    ): Property? {
        return properties!!.stream()
            .filter { e: Property? -> e?.name == propertyName }
            .findFirst().orElseThrow()
    }

    companion object : KLogging() {
        const val DEPARTMENT_PROPERTY_NAME: String = "department"
        const val PACKAGE_NAME: String = "fr.systao.dynostore.example"
        const val EMPLOYEE_ENTITY_NAME: String = "Employee"
        const val DEPARTMENT_ENTITY_NAME: String = "Department"
        const val NAME_PROPERTY_NAME: String = "name"
        const val WORK_BUDDIES_PROPERTY_NAME = "workBuddies"
    }
}
