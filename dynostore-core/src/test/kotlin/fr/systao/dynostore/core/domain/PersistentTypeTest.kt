/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.systao.dynostore.core.domain

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.*
import java.util.stream.Stream

class PersistentTypeTest {

    @ParameterizedTest
    @MethodSource("testIsPersistedParameters")
    fun testIsPersisted(input: PersistentType?, expectedResult: Boolean) {
        assertThat(PersistentType.isPersisted(input)).isEqualTo(expectedResult)
    }

    @ParameterizedTest
    @MethodSource("testUserDoesNotMatchParameters")
    fun `PersistentType#userDoesNotMatch should return true if and only input is persistent (has id) and createdBy doesn't match the principal`(input: PersistentType, principal: String, expectedResult: Boolean) {
        assertThat(PersistentType.userDoesNotMatch(input, principal)).isEqualTo(expectedResult)
    }

    companion object {
        @JvmStatic
        fun testUserDoesNotMatchParameters(): Stream<Arguments> {

            val persistentWithCreatorTest = T()
            persistentWithCreatorTest.id = UUID.randomUUID()
            persistentWithCreatorTest.createdBy = "test"

            val notPersistentWithCreatorTest = T()
            notPersistentWithCreatorTest.createdBy = "test"

            val notPersistent = T()

            return Stream.of(
                Arguments.of(persistentWithCreatorTest, "test", false),
                Arguments.of(persistentWithCreatorTest, "test2", true),
                Arguments.of(notPersistentWithCreatorTest, "test", false),
                Arguments.of(notPersistentWithCreatorTest, "test2", false),
                Arguments.of(notPersistent, "test", false)
            )
        }

        @JvmStatic
        fun testIsPersistedParameters() : Stream<Arguments> {
            val persistent = T()
            persistent.id = UUID.randomUUID()

            return Stream.of(
                Arguments.of(null, false),
                Arguments.of(persistent, true),
                Arguments.of(T(), false),
                Arguments.of(T(UUID.randomUUID()), true)
            )
        }
    }

    class T : PersistentType {
        constructor()
        constructor(id: UUID) { this.id = id }
    }
}