
package fr.systao.dynostore.core.domain.entity.service

/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import assertk.assertAll
import assertk.assertThat
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.SecurityFixtures
import fr.systao.dynostore.core.domain.entity.ScopeExistsException
import fr.systao.dynostore.core.domain.entity.ScopeNotFoundException
import fr.systao.dynostore.core.domain.entity.model.Scope
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class ScopeServiceTest {

    @BeforeEach
    fun setUp() {
        SecurityFixtures.initializeDynostorePrincipal()
    }

    @Test
    fun `Trying to save an existing Scope throws a ScopeExistsException`() {
        val service = ScopeService(mockk(relaxed = true))

        every { service.scopeRepository.findByNameAndCreatedBy("scope", any()) } throws ScopeExistsException("scope")

        assertAll {
            val exception = assertThrows<ScopeExistsException> { service.createScope("scope") }
            assertThat(exception.name).isEqualTo("scope")
        }
    }

    @Test
    fun `Calling createScope calls scopeRepository#save`() {
        val service = ScopeService(mockk(relaxed = true))

        val created = Scope("scope")
        every { service.scopeRepository.findByNameAndCreatedBy("scope", "user-id") } returns null
        every { service.scopeRepository.save(any()) } returns created

        val scope = service.createScope("scope")
        val argument = slot<Scope>()

        assertAll {
            assertThat(scope).isEqualTo(created)
            verify { service.scopeRepository.save(capture(argument)) }
            assertThat(argument.captured.name).isEqualTo("scope")
        }
    }

    @Test
    fun `Calling deleteScope calls scopeRepository#delete`() {
        val service = ScopeService(mockk(relaxed = true))
        val scope = Scope("test")
        val id = UUID.randomUUID()
        scope.id = id
        every { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns scope
        service.deleteScope(id)
        verify { service.scopeRepository.deleteById(id) }
    }

    @Test
    fun `Calling deleteScope with an id referencing a non existing scope throws ScopeNotFoundException`() {
        val service = ScopeService(mockk(relaxed = true))
        every { service.scopeRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertAll {
            assertThrows<ScopeNotFoundException> { service.deleteScope(UUID.randomUUID()) }
            verify(exactly = 0) { service.scopeRepository.deleteById(any()) }
        }
    }

    @Test
    fun `Calling updateScope calls scopeRepository#save`() {
        val service = ScopeService(mockk(relaxed = true))

        val updated = Scope("any()")

        val id = UUID.randomUUID()
        val scope = Scope("scope")
        scope.id = id

        every { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns scope
        every { service.scopeRepository.save(any()) } returns updated

        val result = service.updateScope(id, "updated")
        val argument = slot<Scope>()

        assertAll {
            assertThat(result).isEqualTo(updated)
            verify { service.scopeRepository.save(capture(argument)) }
            assertThat(argument.captured.name).isEqualTo("updated")
        }
    }

    @Test
    fun `Trying to update a non existing scope throws a ScopeNotFoundException`() {
        val service = ScopeService(mockk(relaxed = true))
        every { service.scopeRepository.findByIdAndCreatedBy(any(), SecurityFixtures.USER_ID) } returns null
        assertAll {
            assertThrows<ScopeNotFoundException> { service.updateScope(UUID.randomUUID(), "updated") }
            verify(exactly = 0) { service.scopeRepository.save(any()) }
        }
    }

    @Test
    fun `loadScopes returns all scopes created by the user`() {
        val service = ScopeService(mockk(relaxed = true))
        val scopes = listOf(Scope("test1"), Scope("test2"))
        every { service.scopeRepository.findByCreatedBy(SecurityFixtures.USER_ID) } returns scopes
        assertAll {
            assertThat(service.loadScopes()).isEqualTo(scopes)
            verify { service.scopeRepository.findByCreatedBy(SecurityFixtures.USER_ID) }
        }
    }

    @Test
    fun `loadScope delegates to repository#findByIdAndCreatedBy`() {
        val service = ScopeService(mockk(relaxed = true))
        val id = UUID.randomUUID()
        val scope = Scope("test1")
        every { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns scope
        assertAll {
            assertThat(service.loadScope(id)).isEqualTo(scope)
            verify { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) }
        }
    }

    @Test
    fun `Trying to retrieve a non existing scope throws a ScopeNotFoundException`() {
        val service = ScopeService(mockk(relaxed = true))
        val id = UUID.randomUUID()
        every { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns null
        assertAll {
            assertThrows<ScopeNotFoundException> { service.loadScope(id) }
            verify { service.scopeRepository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) }
        }
    }
}