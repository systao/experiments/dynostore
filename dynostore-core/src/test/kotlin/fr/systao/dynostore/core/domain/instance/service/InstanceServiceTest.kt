/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.service

import assertk.assertThat
import assertk.assertions.hasSize
import assertk.assertions.isEqualTo
import fr.systao.dynostore.core.SecurityFixtures
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.instance.ForbiddenInstanceAccessException
import fr.systao.dynostore.core.domain.instance.InconsistentInstanceQueryException
import fr.systao.dynostore.core.domain.instance.InstanceNotFoundException
import fr.systao.dynostore.core.domain.instance.PropertyNotFoundException
import fr.systao.dynostore.core.domain.instance.model.Instance
import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SyntheticKey
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.security.access.AccessDeniedException
import java.util.*
import java.util.stream.Stream
import javax.persistence.EntityManager

class InstanceServiceTest {
    @BeforeEach
    fun setUp() {
        SecurityFixtures.initializeDynostorePrincipal()
    }

    @Test
    fun `Calling InstanceService#create(entity, set()) saves a persistent instance of the given StoreEntity`() {
        val service = InstanceService(mockk())
        service.entityManager = mockEntityManager()

        val entity = StoreEntity("test")
        entity.createdBy = "user-id"

        val properties = mutableSetOf<PropertyValue>()
        val instance = Instance(entity, properties)

        val argument = slot<Instance>()
        every { service.repository.save(capture(argument)) } returns instance

        val created = service.create(entity, properties)

        assertThat(created).isEqualTo(instance)
        verify { service.repository.save(any()) }
        assertThat(argument.captured.storeEntity).isEqualTo(entity)
        assertThat(argument.captured.properties).isEqualTo(properties)
    }

    @Test
    fun `Trying to create an instance of an entity that doesn't belong to the current user throws a InconsistentInstanceQueryException`() {
        val service = InstanceService(mockk())

        val entity = StoreEntity("test")
        entity.createdBy = "not the current user"
        entity.id = UUID.randomUUID()

        assertThrows<InconsistentInstanceQueryException> {  service.create(entity, mutableSetOf()) }
        verify(exactly = 0) { service.repository.save(any()) }
    }

    @Test
    fun `Trying to create an instance in a scope that doesn't belong to the current user throws a InconsistentInstanceQueryException`() {
        val service = InstanceService(mockk())

        val entity = StoreEntity("test")
        entity.createdBy = SecurityFixtures.USER_ID
        entity.id = UUID.randomUUID()

        val scope = Scope("test")
        scope.createdBy = "not the current user"
        scope.id = UUID.randomUUID()

        assertThrows<InconsistentInstanceQueryException> {  service.create(scope, entity, mutableSetOf()) }
        verify(exactly = 0) { service.repository.save(any()) }
    }

    @Test
    fun `Calling InstanceService#create(entity, properties) with at least one unknown property throws a PropertyNotFoundException`() {
        val service = InstanceService(mockk())
        service.entityManager = mockEntityManager()

        val entity = StoreEntity("test")
        entity.createdBy = "user-id"

        val properties = mutableSetOf(PropertyValue(Property("test")), PropertyValue(Property("unknown")))

        assertThrows<PropertyNotFoundException> {  service.create(entity, properties) }
        verify(exactly = 0) { service.repository.save(any()) }
    }

    @Test
    fun `Calling InstanceService#addProperties(instance, properties) with at least one unknown property throws a PropertyNotFoundException`() {
        val service = InstanceService(mockk())
        service.entityManager = mockEntityManager()

        val entity = StoreEntity("test")
        entity.createdBy = "user-id"

        val property = Property("test")
        property.primitiveType = PrimitiveType.STRING
        val properties = mutableSetOf(PropertyValue(property), PropertyValue(Property("unknown")))

        val newInstance = Instance(entity, mutableSetOf())
        every { service.repository.save(any()) } returns newInstance

        val instance = service.create(entity, mutableSetOf())

        assertThrows<PropertyNotFoundException> {  service.addProperties(instance!!, properties) }
    }

    @Test
    fun `Calling InstanceService#addProperties(instance, properties) with known properties augments the instance properties`() {
        val service = InstanceService(mockk())
        service.entityManager = mockEntityManager()

        val entity = StoreEntity("test")
        entity.createdBy = "user-id"

        val property = Property("test")
        property.primitiveType = PrimitiveType.STRING
        entity.addProperty(property)

        val properties = mutableSetOf(PropertyValue(property).setValue("test1"))

        val newInstance = Instance(entity, mutableSetOf())
        every { service.repository.save(any()) } returns newInstance

        val instance = service.create(entity, mutableSetOf())

        service.addProperties(instance!!, properties)

        assertThat(instance.properties).hasSize(1)
        val propertyValue = instance.properties.first()
        assertThat(propertyValue.property?.name).isEqualTo("test")
        assertThat(propertyValue.singleValue?.getValue()).isEqualTo("test1")
    }

    @Test
    fun `Trying to add properties to an instance that doesn't belong to the current user throws a ForbiddenInstanceAccessException`() {
        val service = InstanceService(mockk())

        val instance = Instance()
        val id = UUID.randomUUID()
        instance.id = id
        instance.createdBy = "not the current user"

        every { service.repository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns instance
        assertThrows<ForbiddenInstanceAccessException> {  service.addProperties(instance, mutableSetOf()) }
    }

    @ParameterizedTest
    @MethodSource("testConsistencyValidationParameters")
    fun testFindConsistencyValidation(instance: Instance, scopeIdArg: UUID, entityIdArg: UUID, exception: Boolean) {
        val service = InstanceService(mockk())
        val instanceId = instance.id!!
        every { service.repository.findByIdAndCreatedBy(instanceId, SecurityFixtures.USER_ID) } returns instance

        if ( exception ) {
            assertThrows<InconsistentInstanceQueryException> { service.find(SyntheticKey(scopeIdArg, entityIdArg, instanceId)) }
        }
        else {
            assertThat(service.find(SyntheticKey(scopeIdArg, entityIdArg, instanceId))).isEqualTo(instance)
        }
    }

    @Test
    fun `Trying to retrieve a non existing instance throws an InstanceNotFoundException`() {
        val service = InstanceService(mockk())
        val id = UUID.randomUUID()
        every { service.repository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns null
        assertThrows<InstanceNotFoundException> { service.find(SyntheticKey(UUID.randomUUID(), UUID.randomUUID(), id)) }
    }

    @Test
    fun `Trying to retrieve an instance that doesn't belong to the current user throws an AccessDeniedException`() {
        val service = InstanceService(mockk())
        val id = UUID.randomUUID()
        SecurityFixtures.initializeAnonymousUser()
        assertThrows<AccessDeniedException> { service.find(SyntheticKey(UUID.randomUUID(), UUID.randomUUID(), id)) }
    }

    @ParameterizedTest
    @MethodSource("testConsistencyValidationParameters")
    fun testDeleteConsistencyValidation(instance: Instance, scopeIdArg: UUID, entityIdArg: UUID, exception: Boolean) {
        val service = InstanceService(mockk(relaxUnitFun = true, relaxed = true))
        val instanceId = instance.id!!
        every { service.repository.findByIdAndCreatedBy(instanceId, SecurityFixtures.USER_ID) } returns instance

        if ( exception ) {
            assertThrows<InconsistentInstanceQueryException> { service.delete(SyntheticKey(scopeIdArg, entityIdArg, instanceId)) }
        }
        else {
            assertThat(service.delete(SyntheticKey(scopeIdArg, entityIdArg, instanceId)))
            verify { service.repository.delete(instance) }
        }
    }

    @Test
    fun `Trying to delete a not existing Instance throws an InstanceNotFoundException`() {
        val service = InstanceService(mockk())
        val id = UUID.randomUUID()
        every { service.repository.findByIdAndCreatedBy(id, SecurityFixtures.USER_ID) } returns null
        assertThrows<InstanceNotFoundException> { service.find(SyntheticKey(UUID.randomUUID(), UUID.randomUUID(), id)) }
    }

    private fun mockEntityManager() : EntityManager {
        val entityManager = mockk<EntityManager>()
        every { entityManager.merge<Any>(any()) } answers { arg<StoreEntity>(0) }
        return entityManager
    }

    companion object {
        @JvmStatic
        fun testConsistencyValidationParameters() : Stream<Arguments> {
            val entityId = UUID.randomUUID()
            val scopeId = UUID.randomUUID()
            val instanceId = UUID.randomUUID()

            val entity = StoreEntity("test")
            val scope = Scope("test")

            scope.id = scopeId
            entity.id = entityId
            entity.setOwner(scope)

            val instance = Instance(entity)
            instance.id = instanceId
            instance.owner = scope

            return Stream.of(
                Arguments.of(instance, scopeId, entityId, false),
                Arguments.of(instance, scopeId, UUID.randomUUID(), true),
                Arguments.of(instance, UUID.randomUUID(), entityId, true),
                Arguments.of(instance, UUID.randomUUID(), UUID.randomUUID(), true),
            )
        }
    }


}