/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.service

import fr.systao.dynostore.core.domain.PersistentType
import fr.systao.dynostore.core.domain.entity.*
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.PropertyKey
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.repository.ScopeRepository
import fr.systao.dynostore.core.domain.entity.repository.StoreEntityRepository
import fr.systao.dynostore.core.security.DynostorePrincipal
import fr.systao.dynostore.core.service.DynostoreService
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.stream.Stream
import kotlin.streams.toList

@Service
@PreAuthorize(DynostorePrincipal.IS_AUTHENTICATED)
class StoreEntityService(
    var storeEntityRepository: StoreEntityRepository,
    var scopeRepository: ScopeRepository,
    val listeners: List<StoreEntityListener> = listOf()) : DynostoreService(), ScopeListener {

    @Transactional
    fun create(name: String, owner: Scope, properties: MutableSet<Property>) : StoreEntity {
         return ifValidAuthentication { principal ->
            return@ifValidAuthentication if (!PersistentType.isPersisted(owner) || scopeRepository.findByIdAndCreatedBy(
                    owner.id!!, principal.id()) is Scope ) {
                val storeEntity = StoreEntity(name)
                storeEntity.properties = properties
                storeEntity.setOwner(owner)
                storeEntityRepository.save(storeEntity)
            }
            else {
                throw AccessDeniedException("Invalid credentials")
            }
        }
    }

    @Transactional
    fun create(name: String, ownerId: UUID, properties: MutableSet<Property>): StoreEntity {
        return ifValidAuthentication { principal ->
            return@ifValidAuthentication when (val owner = scopeRepository.findByIdAndCreatedBy(ownerId, principal.id())) {
                null -> throw ScopeNotFoundException(ownerId)
                else -> create(name, owner, properties)
            }
        }
    }

    @Transactional
    fun delete(id: UUID, scopeId: UUID?) {
        return ifValidAuthentication { principal ->
            when (val entity = storeEntityRepository.findByIdAndCreatedBy(id, principal.id()) ) {
                null -> throw EntityNotFoundException(id)
                else -> {
                    validateConsistency(entity, scopeId)
                    fireEvent(DeleteEntityEvent(entity))
                    storeEntityRepository.deleteById(id)
                }
            }
        }
    }

    @Transactional(readOnly = true)
    fun find(name: String, ownerId: UUID) : StoreEntity? {
        return ifValidAuthentication { principal ->
            return@ifValidAuthentication storeEntityRepository.findByOwnerIdAndNameAndCreatedBy(ownerId, name, principal.id()) ?: throw EntityNotFoundException(name, ownerId)
        }
    }

    @Transactional(readOnly = true)
    fun find(scopeId: UUID, id: UUID) : StoreEntity? {
        return ifValidAuthentication { principal ->
            return@ifValidAuthentication storeEntityRepository.findByOwnerIdAndIdAndCreatedBy(scopeId, id, principal.id()) ?: throw EntityNotFoundException(id, scopeId)
        }
    }

    @Transactional
    fun addProperties(entityId: UUID, scopeId: UUID, properties: List<Property>) : StoreEntity {
        return ifValidAuthentication { principal ->
            when (val entity = storeEntityRepository.findByIdAndCreatedBy(entityId, principal.id()) ) {
                null -> throw EntityNotFoundException(entityId)
                else -> {
                    validateConsistency(entity, scopeId)
                    validateNewProperties(entity, properties)
                    properties.forEach { entity.addProperty(it) }
                    return@ifValidAuthentication storeEntityRepository.save(entity)
                }
            }
        }
    }

    @Transactional
    fun removeProperties(entityId: UUID, scopeId: UUID, propertyNames: List<String>) : StoreEntity {
        return ifValidAuthentication { principal ->
            when (val entity = storeEntityRepository.findByIdAndCreatedBy(entityId, principal.id())) {
                null -> throw EntityNotFoundException(entityId)
                else -> {
                    validateConsistency(entity, scopeId)
                    validatePropertiesToRemove(entity, propertyNames)
                    fireEvent(DeletePropertyEvent(entity, propertyNames.toSet()))
                    propertyNames.forEach { entity.removeProperty(it) }
                    return@ifValidAuthentication storeEntityRepository.save(entity)
                }
            }
        }
    }

    @Transactional
    fun rename(entityId: UUID, scopeId: UUID, name: String) : StoreEntity {
        return ifValidAuthentication { principal ->
            when (val entity = storeEntityRepository.findByIdAndCreatedBy(entityId, principal.id())) {
                null -> throw EntityNotFoundException(entityId)
                else -> {
                    validateConsistency(entity, scopeId)
                    entity.name = name
                    return@ifValidAuthentication storeEntityRepository.save(entity)
                }
            }
        }
    }

    @Transactional(readOnly = true)
    fun findByScope(scopeId: UUID): Stream<StoreEntity>? {
        return ifValidAuthentication { principal ->
            return@ifValidAuthentication this.storeEntityRepository.findByOwnerIdAndCreatedBy(scopeId, principal.id())
        }
    }

    @Transactional
    fun renameProperty(propertyKey: PropertyKey, name: String) : Property {
        val entityId = propertyKey.entityId
        val scopeId = propertyKey.scopeId
        val propertyId = propertyKey.propertyId

        return ifValidAuthentication { principal ->
            when (val entity = storeEntityRepository.findByIdAndCreatedBy(entityId, principal.id())) {
                null -> throw EntityNotFoundException(entityId)
                else -> {
                    validateConsistency(entity, scopeId)
                    val property = findProperty(entity, propertyId, name)
                    property.name = name
                    storeEntityRepository.save(entity)
                    return@ifValidAuthentication property
                }
            }
        }
    }

    @Transactional
    override fun onEvent(event: DeleteScopeEvent) {
        val scope = event.scope
        if (PersistentType.isPersisted(scope)) {
            val entities = storeEntityRepository.findByOwnerId(scope.id).toList()
            entities.forEach { fireEvent(DeleteEntityEvent(it)) }
            storeEntityRepository.deleteAll(entities)
        }
    }

    private fun findProperty(entity: StoreEntity, propertyId: UUID, name: String): Property {
        val matches = entity.properties.filter { it.id == propertyId }
        if (matches.isEmpty()) {
            throw NoSuchPropertyException(propertyId, entity.resolveName())
        }

        val conflicts = entity.properties.filter { it.id != propertyId && it.name == name }
        if (conflicts.isNotEmpty()) {
            throw PropertyConflictException(propertyId, listOf(name))
        }

        return matches[0]
    }

    private fun validateConsistency(entity: StoreEntity, scopeId: UUID?) {
        if (scopeId != entity.getOwner()?.id) {
            throw InconsistentQueryException("Entity ${entity.id} doesn't belong to scope $scopeId")
        }
    }

    private fun validateNewProperties(entity: StoreEntity, newProperties: List<Property>) {
        val entityPropertyNames = entity.properties.map(Property::name).toSet()
        val newPropertyNames = newProperties.map(Property::name).toList().filterNotNull()

        val intersection = entityPropertyNames.intersect(newPropertyNames.toSet())
        if (intersection.isNotEmpty()) {
            throw PropertyConflictException(entity.id!!, newPropertyNames)
        }

        if (newPropertyNames.size - newPropertyNames.distinct().size != 0) {
            throw InconsistentQueryException("Trying to add homonym properties")
        }
    }

    private fun validatePropertiesToRemove(entity: StoreEntity, propertyNames: List<String>) {
        val entityPropertyNames = entity.properties.map(Property::name).toSet()

        val intersection = entityPropertyNames.intersect(propertyNames.toSet())
        if (intersection.isEmpty()) {
            throw InconsistentQueryException("Trying to remove non existent properties")
        }

        if (propertyNames.size - propertyNames.distinct().size != 0) {
            throw InconsistentQueryException("Trying to remove same property multiple times")
        }
    }

    private fun fireEvent(event: DeleteEntityEvent) {
        this.listeners.forEach { it.onEvent(event) }
    }

    private fun fireEvent(event: DeletePropertyEvent) {
        this.listeners.forEach { it.onEvent(event) }
    }
}
