/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity

import java.util.*

open class ModelException(var id: UUID?) : RuntimeException() {
    var name : String? = null
    constructor(name: String?) : this(null as UUID?) {
        this.name = name
    }
}

class ScopeNotFoundException : ModelException {
    constructor(name: String?) : super(name)
    constructor(id: UUID?): super(id)
}

class ScopeExistsException(name: String) : ModelException(name) { }

class EntityNotFoundException : ModelException {
    private var ownerId : UUID? = null
    constructor(id: UUID?): super(id)
    constructor(name: String?, ownerId: UUID?): super(name) {
        this.ownerId = ownerId
    }
    constructor(id: UUID?, ownerId: UUID?): this(id) {
        this.ownerId = ownerId
    }
}

class PropertyConflictException(var entityId : UUID?, var properties: Collection<String>) : ModelException(entityId)

class NoSuchPropertyException(name: String, var entity : String) : ModelException(name) {
    constructor(id: UUID, entity: String) : this(id.toString(), entity)
}

class InconsistentModelException(message : String) : RuntimeException(message)

class InconsistentQueryException(message : String) : RuntimeException(message)