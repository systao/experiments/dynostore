/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.model

import fr.systao.dynostore.core.domain.PersistentType
import fr.systao.dynostore.core.domain.entity.NoSuchPropertyException
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import javax.persistence.CascadeType
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@javax.persistence.Entity(name = "Instance")
class Instance() : PersistentType() {

    constructor(storeEntity: StoreEntity) : this() { this.storeEntity = storeEntity }
    constructor(storeEntity: StoreEntity, properties : MutableSet<PropertyValue>) : this(storeEntity) { this.properties = properties }

    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST])
    var storeEntity: StoreEntity? = null

    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    var properties: MutableSet<PropertyValue> = mutableSetOf()

    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST])
    var owner: Scope? = null

    fun addProperty(property: PropertyValue): Instance {
        property.owner = this
        properties.add(property)
        return this
    }

    fun move(owner: Scope?): Instance {
        this.owner = owner
        return this
    }

    fun getProperty(name: String?): Any? {
        return properties.stream()
                .filter { pv: PropertyValue? -> pv?.getName() == name }
                .findFirst()
                .map { pv ->
                    return@map pv.getValue()
                }
                .orElse(null)
    }

    fun setProperty(name: String, value: Any?): Instance {
        properties.stream()
            .filter { pv: PropertyValue? -> pv?.getName() == name }
            .findFirst()
            .ifPresentOrElse({ pv ->
                updatePropertyValue(pv, value)
            }, {
                addProperty(name, value)
            })

        return this
    }

    fun resetProperty(name: String?) : Instance {
        val matches = properties
            .filter { pv: PropertyValue? -> pv?.getName() == name }
            .toList()
        properties.removeAll(matches.toSet())
        return this
    }

    internal fun getPropertyObjectWrapper(name: String) : PropertyValue? = properties.stream()
            .filter { pv: PropertyValue? -> pv?.getName() == name }
            .findFirst()
            .orElse(null)

    private fun addProperty(name: String, value: Any?) {
        storeEntity
            ?.properties
            ?.stream()
            ?.filter { p: Property? -> p?.name == name }
            ?.findFirst()
            ?.ifPresentOrElse({ p ->
                value?.let { this.addProperty(PropertyValue(p).setValue(it)) }
            }, {
                throw storeEntity?.name?.let { NoSuchPropertyException(name, it) }!!
            })
    }

    private fun updatePropertyValue(pv: PropertyValue, value: Any?) {
        val multiplicity = pv.property?.multiplicity

        if (multiplicity == "1") {
            if ( pv.singleValue == null ) {
                val singleValue = SingleValue()
                singleValue.propertyValue = pv
                pv.singleValue = singleValue
            }
            pv.singleValue!!.setValue(value)
        } else {
            pv.multiValue?.clear()
            if (value is Collection<*>) {
                value.forEach {
                    val singleValue = SingleValue()
                    singleValue.propertyValue = pv
                    singleValue.setValue(it)
                    pv.multiValue?.add(singleValue)
                }
            }
        }
    }
}