/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.model

import fr.systao.dynostore.core.domain.PersistentType
import fr.systao.dynostore.core.domain.entity.InconsistentModelException
import fr.systao.dynostore.core.domain.entity.model.PrimitiveType
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import java.math.BigDecimal
import javax.persistence.*

@Entity(name = "SingleValue")
class SingleValue(var stringValue: String? = null,
                  var decimalValue: BigDecimal? = null,
                  var booleanValue: Boolean? = null,
                  var integerValue: Int? = null,
                  @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST]) var reference: Instance? = null
) : PersistentType() {
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST])
    var propertyValue: PropertyValue? = null

    @Suppress("UNREACHABLE_CODE")
    fun getValue(): Any? {
        if (propertyValue?.property == null) {
            return throw InconsistentModelException("SingleValue.propertyValue must not be null ")
        }

        return when (propertyValue!!.property!!.primitiveType) {
            PrimitiveType.STRING -> stringValue
            PrimitiveType.BOOLEAN -> booleanValue
            PrimitiveType.DECIMAL -> decimalValue
            PrimitiveType.INTEGER -> integerValue
            else -> reference
        }
    }

    @Suppress("UNREACHABLE_CODE")
    fun setValue(value: Any?): SingleValue
    {
        if (propertyValue?.property == null) {
            return throw InconsistentModelException("SingleValue.propertyValue must not be null ")
        }

        when (propertyValue!!.property!!.primitiveType) {
            PrimitiveType.STRING -> this.stringValue = value as String
            PrimitiveType.BOOLEAN -> this.booleanValue = value as Boolean
            PrimitiveType.DECIMAL -> this.decimalValue = value as BigDecimal
            PrimitiveType.INTEGER -> this.integerValue = value as Int
            else -> setReference(value)
        }

        return this
    }

    private fun setReference(value: Any?) {
        val targetEntity = this.getType()

        if (value == null || value is Instance? && value.storeEntity == targetEntity) {
            this.reference = value as Instance?
        }
        else {
            throw ClassCastException("Can't cast $value to $targetEntity")
        }
    }

    private fun getType() : StoreEntity? {
        return propertyValue?.property?.storeEntity
    }
}