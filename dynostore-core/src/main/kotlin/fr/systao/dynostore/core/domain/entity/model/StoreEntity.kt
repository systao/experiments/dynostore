/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.model

import fr.systao.dynostore.core.domain.entity.NoSuchPropertyException
import fr.systao.dynostore.core.domain.entity.PropertyConflictException
import javax.persistence.*
import kotlin.streams.toList

@Entity(name = "Entity")
class StoreEntity(name: String?) : NamedElement(name) {
    constructor() : this(null)

    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true)
    var properties: MutableSet<Property> = mutableSetOf()

    fun getFullyQualifiedName() : String {
        return if ( owner == null ) name!!
        else owner!!.name + "." + name
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.PERSIST])
    private var owner: Scope? = null
    fun setOwner(value: Scope?) {
        this.owner?.remove(this)
        value?.let {
            this.owner = it
            if (!this.owner!!.contains(this)) {
                this.owner?.add(this)
            }
        }
    }
    fun getOwner() : Scope? {
        return owner
    }

    fun addProperty(property: Property): StoreEntity {
        if ( hasProperty(property) ) {
            throw PropertyConflictException(id, getPropertyNames())
        }
        properties.add(property)
        return this
    }

    fun getProperty(name: String): Property {
        return properties.stream()
                .filter { p: Property -> name == p.name }
                .findFirst()
                .orElseThrow { NoSuchPropertyException(name, resolveName()) }
    }

    fun hasProperty(property: Property) : Boolean {
        return hasProperty(property.name!!)
    }

    fun removeProperty(propertyName: String) {
        if ( hasProperty(propertyName) ) {
            val property = this.getProperty(propertyName)
            this.properties.remove(property)
            property.setStoreEntity(null)
        }
        else {
            throw NoSuchPropertyException(propertyName, resolveName())
        }
    }

    fun hasProperty(name: String) : Boolean {
        return properties.stream()
            .filter { p: Property -> name == p.name }
            .findFirst().isPresent
    }

    private fun getPropertyNames() : List<String> {
        @Suppress("UNCHECKED_CAST")
        return properties.stream().map(Property::name).toList() as List<String>
    }

    fun resolveName() : String {
        return this.name ?: "Nil"
    }
}