/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.model

import fr.systao.dynostore.core.domain.entity.InconsistentModelException
import javax.persistence.ManyToOne

@javax.persistence.Entity(name = "Property")
class Property(name: String?) : NamedElement(name) {
    constructor() : this(null)

    var multiplicity = "1"
    set(value) {
        validateMultiplicity(value)
        field = value
    }

    @ManyToOne
    var owner: StoreEntity? = null

    @ManyToOne
    @JvmField var storeEntity: StoreEntity? = null

    @JvmField var primitiveType: PrimitiveType? = null

    fun setStoreEntity(value: StoreEntity?) {
        if ( this.storeEntity != value ) {
            this.storeEntity = value
            this.storeEntity?.let {
                if ( !it.hasProperty(this) ) {
                    it.addProperty(this)
                }
            }
        }
    }

    fun setType(type: StoreEntity): Property {
        storeEntity = type
        primitiveType = null
        return this
    }

    fun setPrimitiveType(primitiveType: PrimitiveType): Property {
        this.primitiveType = primitiveType
        storeEntity = null
        return this
    }

    fun setPropertyMultiplicity(multiplicity: String): Property {
        this.multiplicity = multiplicity
        return this
    }

    fun unbind(): Property {
        this.multiplicity = "*"
        return this
    }

    private fun validateMultiplicity(multiplicity: String) {
        val v = multiplicity.toIntOrNull()
        if (v is Int && v > 9999 || v == null && multiplicity != "*") {
            throw InconsistentModelException("multiplicity must '*' or Int < 10000")
        }
    }


}