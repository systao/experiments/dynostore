/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.model

import fr.systao.dynostore.core.domain.PersistentType
import fr.systao.dynostore.core.domain.entity.model.Property
import javax.persistence.*

@Entity(name = "PropertyValue")
class PropertyValue(@ManyToOne var property: Property?) : PersistentType() {

    @ManyToOne
    var owner: Instance? = null

    @OneToOne(cascade = [CascadeType.ALL], mappedBy = "propertyValue")
    var singleValue: SingleValue? = null

    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "propertyValue")
    @JvmField var multiValue: MutableSet<SingleValue>? = null
    fun setMultiValue(value: MutableSet<SingleValue>?) {
        this.multiValue = value
        this.multiValue?.forEach { it.propertyValue = this }
    }

    fun getName(): String? {
        return property?.name
    }

    fun setValue(value: Any) : PropertyValue {
        if ( value is Collection<Any?> ) {
            this.multiValue = mutableSetOf()
            value.forEach{
                this.multiValue!!.add(attach(it!!))
            }
        }
        else {
            this.singleValue = attach(value)
        }
        return this
    }

    fun getValue(): Any? {
        val multiplicity = property?.multiplicity

        return if (multiplicity == "1") {
            singleValue?.getValue()
        } else {
            val values = mutableSetOf<Any?>()
            multiValue?.forEach { values.add(it.getValue()) }
            values
        }
    }

    private fun attach(value: Any) : SingleValue {
        val singleValue = SingleValue()
        singleValue.propertyValue = this
        singleValue.setValue(value)
        return singleValue
    }

}