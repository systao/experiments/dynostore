/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.config

import com.zaxxer.hikari.HikariDataSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource
import javax.sql.DataSource

@Configuration
@ConfigurationProperties
class TenantConfiguration(@Autowired(required = false) val tenantHolder : TenantHolder?, @Autowired(required = false) val initializer : SchemaInitializer?) {

    var tenants : Map<String, Tenant>? = null

    @Bean
    @Primary
    fun dataSource(): DataSource {
        val routingDataSource = DataSourceRouting(this.tenantHolder)
        val dataSources = mutableMapOf<String, DataSource?>()
        tenants?.forEach {
            val ds = it.value.dataSource
                ?.initializeDataSourceBuilder()
                ?.type(HikariDataSource::class.java)
                ?.build()
            dataSources[it.key] = ds
            initializer?.init(ds!!)
        }
        routingDataSource.initDatasource(dataSources)
        return routingDataSource
    }
}

class Tenant {
    var dataSource : DataSourceProperties? = null
    var name :  String? = null
}

interface TenantHolder {
    fun tenant() : String
}

interface SchemaInitializer {
    fun init(ds: DataSource)
}

class DataSourceRouting(private var tenantHolder : TenantHolder?) : AbstractRoutingDataSource() {
    override fun determineCurrentLookupKey(): String {
        return tenantHolder?.tenant() ?: PUBLIC
    }

    @Suppress("UNCHECKED_CAST")
    fun initDatasource(dataSources :Map<String, DataSource?>) {
        setTargetDataSources(dataSources as Map<Any?, Any?>)
        dataSources[PUBLIC]?.let { setDefaultTargetDataSource(it) }
    }

    companion object {
        const val PUBLIC : String = "public"
    }
}