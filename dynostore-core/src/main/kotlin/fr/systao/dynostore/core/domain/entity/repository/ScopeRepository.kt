/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.repository

import fr.systao.dynostore.core.domain.entity.model.Scope
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface ScopeRepository : JpaRepository<Scope, UUID> {
    fun findByNameAndCreatedBy(name: String?, creator: String): Scope?

    fun findByCreatedBy(id: String) : List<Scope>

    fun findByIdAndCreatedBy(id: UUID, creator: String) : Scope?
}