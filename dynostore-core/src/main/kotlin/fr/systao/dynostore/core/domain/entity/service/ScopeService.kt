/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.service

import fr.systao.dynostore.core.domain.entity.ScopeExistsException
import fr.systao.dynostore.core.domain.entity.ScopeNotFoundException
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.repository.ScopeRepository
import fr.systao.dynostore.core.security.DynostorePrincipal
import fr.systao.dynostore.core.service.DynostoreService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@PreAuthorize(DynostorePrincipal.IS_AUTHENTICATED)
class ScopeService(var scopeRepository: ScopeRepository, val listeners: List<ScopeListener> = listOf()) : DynostoreService() {

    @Transactional
    fun createScope(name: String) : Scope {
        return ifValidAuthentication { principal ->
            when (scopeRepository.findByNameAndCreatedBy(name, principal.id())) {
                null -> return@ifValidAuthentication scopeRepository.save(Scope(name))
                else -> throw ScopeExistsException(name)
            }
        }
    }

    @Transactional(readOnly = true)
    fun loadScopes() : List<Scope> {
        return ifValidAuthentication { principal -> scopeRepository.findByCreatedBy(principal.id()) }
    }

    @Transactional(readOnly = true)
    fun loadScope(id: UUID): Scope? {
        return ifValidAuthentication { principal ->
            val scope = scopeRepository.findByIdAndCreatedBy(id, principal.id())
            return@ifValidAuthentication scope ?: throw ScopeNotFoundException(id)
        }
    }

    @Transactional
    fun deleteScope(id: UUID) {
        ifValidAuthentication { principal ->
            when (val scope = scopeRepository.findByIdAndCreatedBy(id, principal.id())) {
                null -> throw ScopeNotFoundException(id)
                else -> {
                    listeners.forEach { it.onEvent(DeleteScopeEvent(scope)) }
                    scopeRepository.deleteById(id)
                }
            }
        }
    }

    @Transactional
    fun updateScope(id: UUID, name: String): Scope {
        return ifValidAuthentication { principal ->
            when (val scope = scopeRepository.findByIdAndCreatedBy(id, principal.id())) {
                null -> throw ScopeNotFoundException(id)
                else -> {
                    scope.name = name
                    return@ifValidAuthentication scopeRepository.save(scope)
                }
            }
        }
    }

}


