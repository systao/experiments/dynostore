/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.entity.model

import javax.persistence.CascadeType
import javax.persistence.FetchType
import javax.persistence.OneToMany

@javax.persistence.Entity(name ="Scope")
class Scope(name: String?) : NamedElement(name) {
    constructor() : this(null)

    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL], mappedBy = "owner")
    var entities: MutableSet<StoreEntity?> = mutableSetOf()

    fun add(storeEntity: StoreEntity): Scope {
        entities.add(storeEntity)
        if ( storeEntity.getOwner() != this ) {
            storeEntity.setOwner(this)
        }
        return this
    }

    fun remove(entity: StoreEntity) {
        entities.remove(entity)
    }

    fun contains(entity: StoreEntity) : Boolean {
        return entities.contains(entity)
    }
}