/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance

import java.util.*

open class InstanceException(message: String?) : RuntimeException(message) {
    constructor() : this(null)
}

class PropertyNotFoundException(val unmappedProperties: MutableList<String>) : InstanceException(
    String.format("Unmapped properties: %s", unmappedProperties)
)

class InconsistentInstanceQueryException(message: String) : InstanceException(message) {
    constructor(scopeId: UUID?, entityId: UUID) : this(
        String.format("Scope (%s) or Entity (%s) mismatch", scopeId, entityId))

    constructor(scopeId: UUID?, entityId: UUID, instanceId: UUID) : this(
        String.format("Scope (%s) or Entity (%s) mismatch (instance: %s)", scopeId, entityId, instanceId))
}


class ForbiddenInstanceAccessException(val instanceId: UUID?) : InstanceException()

class InstanceNotFoundException(val instanceId: UUID) : InstanceException()