/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain

import org.hibernate.annotations.GenericGenerator
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class PersistentType {
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "uuid")
    var id: UUID? = null

    @CreatedBy
    lateinit var createdBy: String

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    lateinit var createdDate: Date

    @LastModifiedBy
    lateinit var lastModifiedBy: String

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    lateinit var lastModifiedDate: Date

    companion object {
        @JvmStatic
        fun isPersisted(o: PersistentType?) : Boolean {
            return o?.id != null
        }

        @JvmStatic
        fun userDoesNotMatch(o: PersistentType?, principal: String) : Boolean {
            return isPersisted(o) && principal != o?.createdBy
        }
    }

}
