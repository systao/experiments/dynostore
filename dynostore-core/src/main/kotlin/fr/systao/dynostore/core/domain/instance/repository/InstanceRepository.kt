/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.repository

import fr.systao.dynostore.core.domain.instance.model.Instance
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*
import java.util.stream.Stream

interface InstanceRepository : JpaRepository<Instance, UUID> {
    fun findByStoreEntityId(id: UUID): Stream<Instance>
    fun findByIdAndCreatedBy(instanceId: UUID, principal: String): Instance?
}