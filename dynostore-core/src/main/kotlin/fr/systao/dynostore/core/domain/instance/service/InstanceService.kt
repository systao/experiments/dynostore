/*
 * Copyright (c) 2022 Systao
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.systao.dynostore.core.domain.instance.service

import fr.systao.dynostore.core.domain.PersistentType
import fr.systao.dynostore.core.domain.PersistentType.Companion.userDoesNotMatch
import fr.systao.dynostore.core.domain.entity.model.Property
import fr.systao.dynostore.core.domain.entity.model.Scope
import fr.systao.dynostore.core.domain.entity.model.StoreEntity
import fr.systao.dynostore.core.domain.entity.service.DeleteEntityEvent
import fr.systao.dynostore.core.domain.entity.service.DeletePropertyEvent
import fr.systao.dynostore.core.domain.entity.service.StoreEntityListener
import fr.systao.dynostore.core.domain.instance.ForbiddenInstanceAccessException
import fr.systao.dynostore.core.domain.instance.InconsistentInstanceQueryException
import fr.systao.dynostore.core.domain.instance.InstanceNotFoundException
import fr.systao.dynostore.core.domain.instance.PropertyNotFoundException
import fr.systao.dynostore.core.domain.instance.model.Instance
import fr.systao.dynostore.core.domain.instance.model.PropertyValue
import fr.systao.dynostore.core.domain.instance.model.SyntheticKey
import fr.systao.dynostore.core.domain.instance.repository.InstanceRepository
import fr.systao.dynostore.core.security.DynostorePrincipal
import fr.systao.dynostore.core.service.DynostoreService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
import java.util.stream.Collectors
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import kotlin.streams.toList

@Service
@PreAuthorize(DynostorePrincipal.IS_AUTHENTICATED)
class InstanceService(var repository: InstanceRepository) : DynostoreService(), StoreEntityListener {
    @PersistenceContext
    lateinit var entityManager : EntityManager

    @Transactional
    fun create(storeEntity: StoreEntity, properties: MutableSet<PropertyValue>): Instance? {
        return create(null, storeEntity, properties)
    }

    @Transactional
    fun create(scope: Scope?, storeEntity: StoreEntity, properties: MutableSet<PropertyValue>): Instance? {
        return ifValidAuthentication { principal ->
            if ( userDoesNotMatch(scope, principal.id()) || userDoesNotMatch(storeEntity, principal.id()) )  {
                storeEntity.id?.let { throw InconsistentInstanceQueryException(scope?.id, it) }
            }
            val entity = entityManager.merge(storeEntity)
            validateProperties(entity, properties)
            val instance = Instance(entity, properties)
            scope?.let { instance.owner = entityManager.merge(scope) }
            return@ifValidAuthentication repository.save(instance)
        }
    }

    @Transactional
    fun addProperties(instance: Instance, properties: MutableSet<PropertyValue>): Instance {
        return ifValidAuthentication { principal ->
            if (userDoesNotMatch(instance, principal.id()) ) {
                throw ForbiddenInstanceAccessException(instance.id)
            }
            validateProperties(instance.storeEntity!!, properties)
            properties.forEach(instance::addProperty)
            return@ifValidAuthentication instance
        }
    }

    @Transactional
    fun find(syntheticKey: SyntheticKey): Instance {
        val instanceId = syntheticKey.instanceId
        return ifValidAuthentication { principal ->
            val instance = repository.findByIdAndCreatedBy(instanceId, principal.id()) ?: throw InstanceNotFoundException(instanceId)
            validateConsistency(instance, syntheticKey.scopeId, syntheticKey.entityId)
            return@ifValidAuthentication instance
        }
    }

    @Transactional
    fun delete(syntheticKey: SyntheticKey) {
        val instance = find(syntheticKey)
        repository.delete(instance)
    }

    @Transactional
    fun removeProperties(instance: Instance, properties: Set<String>) {
        properties.forEach {
            val property = instance.getPropertyObjectWrapper(it)
            if ( property != null ) {
                property.getName()?.let { name ->
                    instance.resetProperty(name)
                    entityManager.remove(property)
                }
            }
        }
    }

    @Transactional
    fun removeProperties(syntheticKey: SyntheticKey, properties: Set<String>) : Instance {
        val instance = find(syntheticKey)
        removeProperties(instance, properties)
        return repository.save(instance)
    }

    @Transactional
    fun setProperty(syntheticKey: SyntheticKey, name: String, value: Any?) : Instance {
        val instance = find(syntheticKey)
        instance.setProperty(name, value)
        return repository.save(instance)
    }

    @Transactional
    override fun onEvent(event: DeleteEntityEvent) {
        val entity = event.entity
        if (PersistentType.isPersisted(entity)) {
            val instances = repository.findByStoreEntityId(entity.id!!)
            repository.deleteAll(instances.toList())
        }
    }

    @Transactional
    override fun onEvent(event: DeletePropertyEvent) {
        val entity = event.entity
        if (PersistentType.isPersisted(entity)) {
            val instances = repository.findByStoreEntityId(entity.id!!).toList()
            instances.forEach { instance ->
                removeProperties(instance, event.properties)
            }
        }
    }

    private fun validateProperties(storeEntity: StoreEntity, properties: MutableSet<PropertyValue>) {
        val entityPropertyNames = storeEntity.properties.stream()
            .map(Property::name)
            .collect(Collectors.toSet())

        val propertyValueNames = properties.stream()
            .map(PropertyValue::getName)
            .collect(Collectors.toSet())

        val unmappedProperties = propertyValueNames - entityPropertyNames
        if (unmappedProperties.isNotEmpty()) {
            @Suppress("UNCHECKED_CAST")
            throw PropertyNotFoundException(unmappedProperties.toMutableList() as MutableList<String>)
        }
    }

    private fun validateConsistency(instance: Instance, scopeId: UUID, entityId: UUID) {
        if (instance.owner?.id != scopeId || instance.storeEntity?.id != entityId) {
            throw if ( instance.id != null ) InconsistentInstanceQueryException(scopeId, entityId, instance.id!!)
            else InconsistentInstanceQueryException(scopeId, entityId)
        }
    }
}
