#!/bin/bash

set -euo

MAX_TRIES=12

function status() {
    response=$(wget --server-response $HEALTH_ENDPOINT 2>&1 | awk '/^  HTTP/{print $2}')
    echo $response
}

HEALTH_ENDPOINT="http://$HOST:8080/actuator/health"

echo "Wait for $HOST to become available..."
sleep 5
while [[ "$(status)" != "200" ]] && [[ $MAX_TRIES != 0 ]]; do
  echo "Waiting for $HOST, $((MAX_TRIES--)) remaining attempts..."
  sleep 5
done

if [ $MAX_TRIES -eq 0 ]
then
  echo "Error: $HOST not available"
  exit 1
fi

echo "$HOST available, running functional tests..."
newman run \
  https://api.getpostman.com/collections/$POSTMAN_DOMAIN_COLLECTION_UUID?apikey=$POSTMAN_API_KEY \
  -e /app/local.postman_environment.json \
  --env-var base_url=http://$HOST:8080

echo "Stopping API server"
curl -X POST http://$HOST:8080/actuator/shutdown