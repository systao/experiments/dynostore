#!/usr/bin/env bash

#
# Copyright (c) 2022 Systao
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

function usage() {
  echo "Usage: ./prepare-release.sh [-a] [-r (major|minor|patch)]"
  echo "Options: "
  echo "  -r,--release  Next version type. Acceptable values: [major, minor, patch]. Default=minor"
  echo "  -a,--apply    Disable dry-run mode and write changes to the repository. Default=false"
}

# check that constraints are met:
#   - release_type (next version) is in (major, minor, patch)
#   - current version is a snapshot
#   - no snapshot dependency is referenced
function verify() {
  echo -n "Checking release type... "
  if [[ ! " ${release_types[*]} " =~ " ${release_type} " ]]; then
      echo "FAIL"
      usage
      exit 1
  fi
  echo "PASS"

  echo -n "Checking that current version is a snapshot... "
  if [[ "$current_version" == "" || ! $current_version =~ ^.*-SNAPSHOT$ ]]; then
    echo "FAIL. current version: $current_version"
  else
    echo "PASS"
  fi

  # check enforcer rules (requireReleaseDeps)
  echo -n "Checking that there's no snapshot dependencies... "
  mvn enforcer:enforce -q -B
  echo "PASS"
}

# compute next_version (development version)
# assume semantic versioning. behaviour otherwise not guaranteed
function get_next_version() {
  next_version=''
  major=$(echo "$release_version" | cut -d. -f1)
  minor=$(echo "$release_version" | cut -d. -f2)
  patch=$(echo "$release_version" | cut -d. -f3)
  case "$release_type" in
    major ) next_version="$((major + 1)).0.0" ;;
    minor ) next_version="$major.$((minor + 1)).0" ;;
    patch ) next_version="$major.$minor.$((patch + 1))" ;;
    * ) echo "Invalid release type: $release_type" && exit ;;
  esac
  echo "$next_version-SNAPSHOT"
}

# set the project version to on the given branch
# the target is pushed to remote if flag -a is set
# @arguments:
#   $1 target branch
#   $2 target version
function set_version() {
  branch=$1
  new_version=$2

  if git show-ref --quiet refs/heads/$branch; then
      git checkout $branch
  else
    git checkout -b $branch
    push_args="-u origin $branch"
  fi

  echo "Working on branch: $(git branch --show-current)"
  echo "Setting version in $branch to $new_version"

  mvn versions:set -q -B -DnewVersion=$new_version -DoldVersion='*'
  git add .
  git commit -m "Set development version to $new_version"

  if [[ "$dry_run" == "" ]]; then
    echo "Mode dry-run is disabled (option -a). Pushing branch to remote"
    git push $push_args
  fi
}

release_types=("major" "minor" "patch")
dry_run="-DdryRun=true"
release_type="minor"

while getopts ar:-: arg; do
  case $arg in
    a ) dry_run="" ;;
    r ) release_type="$OPTARG" ;;
    - ) LONG_OPTARG="${OPTARG#*=}"
        case $OPTARG in
          release ) release_type="$LONG_OPTARG" ;;
          apply   ) dry_run="" ;;
          ''      ) break ;; # "--" terminates argument processing
          *       ) echo "Illegal option --$OPTARG" >&2; exit 2 ;;
        esac ;;
    \? ) exit 2 ;;  # getopts already reported the illegal option
  esac
done
shift $((OPTIND-1)) # remove parsed options and args from $@ list

current_version=`mvn help:evaluate -Dexpression=project.version -q -DforceStdout`

verify

release_version=$(echo $current_version | sed 's/-SNAPSHOT//')
next_version=$(get_next_version)

echo -e "Release version will be: $release_version. \nNext development version will be: $next_version"

git checkout develop && git pull
set_version releases/v$release_version $release_version true
set_version develop $next_version

echo -n "Cleaning up workdir... "
for f in $(find . -name 'pom.xml.versionsBackup'); do rm $f; done

echo "Done! All good!"
