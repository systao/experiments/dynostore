# Dynostore, a dynamic instantiation model

## In a nutshell

Dynostore exposes an API to dynamically create a domain model in a declarative fashion.

The created model can then be instantiated. Both model and model instances are persistent and related. 

Changes to the model shall be automatically propagated to the instance.

## Example

![A simple model example](dynostore-core/doc/assets/images/example-graph.jpg "A simple model")

Please note that the graph above has been generated from an old version runnning against Neo4J Aura. Current version uses plain JPA.

## Build and run

### Building 

```bash
$ SPRING_PROFILES_ACTIVE=local mvn clean install
```

### Running the app

#### Using Maven

To run the app from the source using maven, execute: 

```bash
$ USER_ID_CLAIM=user_id   
$ export SPRING_PROFILES_ACTIVE=local && \
    export DYNOSTORE_OIDC_USER_ID_CLAIM=$USER_ID_CLAIM && \
    cd dynostore-api && \
    mvn spring-boot:run && \
    cd -
```

Please note that USER_ID_CLAIM is the name of the claim representing the user id in the provided JWT (often `sub`). 

JWT is not really verified at the moment (just parsed -- so you can reuse an expired token)

#### Using Docker 

To run the app locally (in memory transient db), execute:

```bash
$ docker run -e SPRING_PROFILES_ACTIVE=local -p 8080:8080 registry.gitlab.com/systao/experiments/dynostore/dynostore-api:v0.1.0
```

You can supply an env file to override properties (not yet documented, but you can dynostore-api/src/main/resources/application[-local].yml).

Mysql Driver is also distributed with the app, so you can override the (tenant) datasource to enable persistent data.  

Please note that at the moment, a user can see and manipulate entities and instances he created and that tenants are not fully implemented yet (only tenant resolution and database routing is implemented)  

### Domain and instances definition

#### Openapi

A minimal Openapi is distributed with the app and can be retrieved [here](http://localhost:8080/swagger-ui/index.html#/).

#### Postman

A postman collection that demonstrates how to action the API is available in functional-tests/ 

To action the collection, you'll need to set the Authorization header (see below) at the collection level.

Example using newman: 

```bash
$ cd functional-tests
$ newman run Dynostore.postman_collection.json  -e local.postman_environment.json
```

#### Example: create a Scope using cUrl

A scope is a sort of package, i.e. a logical grouping of elements (more documentation to come). To create a scope just curl to /domain/scopes

```bash
$ JWT='eyJ0eXAiOiJKV....'
$ curl --location --request POST 'http://localhost:8080/domain/scopes' \
       --header "Authorization: Bearer $JWT" \
       --header 'Content-Type: application/json' \
       --data-raw '{
          "name": "test"
        }'
```


